<?php
include 'data/config.php';
include 'data/setup.php';

$cat = urldecode($_GET['cat']);
// ini_set('display_errors', 1);
if ($cat == 'Caemmun') {
	$categ_id = 1;
} elseif ($cat == 'Caemmun Star') {
	$categ_id = 3;
} elseif ($cat == 'Caemmun Office') {
	$categ_id = 4;
} elseif ($cat == 'Caemmun Masterchef') {
	$categ_id = 5;
} elseif ($cat == 'Caemmun Rooms') {
	$categ_id = 31;
} elseif ($cat == 'Combo') {
	$categ_id = 19;
}


$cat_titulo = mysqli_query($connect, "SELECT cat_titulo, cat_mae FROM tb_categorias WHERE cat_id = '$categ_id'");

while ($tit = mysqli_fetch_assoc($cat_titulo)) {
	$titulo = $tit['cat_titulo'];
}

$sub = urldecode($_GET['sub']);
$sub_2 = urldecode($_GET['sub2']);

if (isset($_GET['busca'])) {
	$busca = $_GET['busca'];
	$titulo = $_GET['busca'];
	$sub = $_GET['busca'];
} else {
	$busca = '';
}
?>

<!DOCTYPE html>
<html>

<head>
	<?php
	include 'includes/head.php';

	$redirect = null;
	if ($idioma == 'pt' && ($sub == 'All' || $sub == 'Todas')) {
		$redirect = URL_BASE_SITE . '/produtos?cat=' . $cat . '&sub=Todos';
	} else if ($idioma == 'ing' && ($sub == 'Todos' || $sub == 'Todas')) {
		$redirect = URL_BASE_SITE . '/produtos?cat=' . $cat . '&sub=All';
	} else if ($idioma == 'esp' && ($sub == 'All' || $sub == 'Todos')) {
		$redirect = URL_BASE_SITE . '/produtos?cat=' . $cat . '&sub=Todas';
	} else if ($idioma == 'fra' && ($sub == 'Tous' || $sub == 'Todos')) {
		$redirect = URL_BASE_SITE . '/produtos?cat=' . $cat . '&sub=Todas';
	}

	if (!is_null($redirect)) {
		echo '<meta http-equiv="refresh" content="0;url=' . $redirect . '">';
	}
	?>

<style type="text/css">
	body.office form.busca input[type="submit"] {
		background: #22b431;
		border-color: #22b431;
	}
	body.office .page-header-section .header-title span {
		color:#22b431;
	}
	body.office .projects-filter-nav li.btn-active a {
		background: #22b431;
		color: white;
	}
	body.office .projects-slider .single-project .seemore {
		background: #22b431;
		color: white;
	}
	body.office .projects-slider .single-project .seemore:hover {
		background: #12c3f4;
	}
	body.office .projects-slider .single-project .project-content h2 {
		color:#22b431;
	}

	body.star form.busca input[type="submit"] {
		background: #9e1930;
		border-color: #9e1930;
		color: white;
	}
	body.star .page-header-section .header-title span {
		color: #9e1930;
	}
	body.star .projects-filter-nav li.btn-active a{
		background: #9e1930;
		color: white;
	}
	
	body.star .projects-slider .single-project .seemore {
		background: #9e1930;
		color: white;
	}
	body.star .projects-slider .single-project .seemore:hover {
		background: #12c3f4;
	}
	body.star .projects-slider .single-project .project-content h2 {
		color:#b5636f;
	}
	

	body.rooms form.busca input[type="submit"] {
		background: #5f58b1;
		border-color: #5f58b1;
		color: white;
	}
	body.rooms .page-header-section .header-title span {
		color:#5f58b1;
	}
	body.rooms .projects-filter-nav li.btn-active a {
		background: #5f58b1;
		color: white;
	}
	body.rooms .projects-slider .single-project .seemore {
		background: #5f58b1;
		color:white;
	}
	body.rooms .projects-slider .single-project .seemore:hover {
		background: #12c3f4;
		color: white;
	}
	body.rooms .projects-slider .single-project .project-content h2 {
		color:#9996d3;
	}
	
	body.Caemmun form.busca input[type="submit"] {
		background: #387dbf;
		border-color: #387dbf;
	}
	body.Caemmun .page-header-section .header-title span {
		color:#387dbf;
	}
	body.Caemmun .projects-filter-nav li.btn-active a {
		color: #fff;
		background: #387dbf;
	}
	
	body.Caemmun .projects-slider .single-project .seemore {
		background: #387dbf;
		color: #fff;
	}
	body.Caemmun .projects-slider .single-project .seemore:hover {
		background: #;
	}
	body.Caemmun .projects-slider .single-project .project-content h2 {
		color:#a0deef;
	}

	body.bancada form.busca input[type="submit"] {
		background: #ea186f;
		border-color: #ea186f;
	}
	body.bancada .page-header-section .header-title span {
		color:#ea186f;
	}
	body.bancada .projects-filter-nav li.btn-active a {
		color: #fff;
		background: #ea186f;
	}
	
	body.bancada .projects-slider .single-project .seemore {
		background: #ea186f;
		color: #fff;
	}
	body.bancada .projects-slider .single-project .seemore:hover {
		background: #;
	}
	body.bancada .projects-slider .single-project .project-content h2 {
		color:#d68ba9;
	}
	
	body.combo form.busca input[type="submit"] {
		background: #ea186f;
		border-color: #ea186f;
	}
	body.combo .page-header-section .header-title span {
		color:#ea186f;
	}
	body.combo .projects-filter-nav li.btn-active a {
		color: #fff;
		background: #ea186f;
	}
	
	body.combo .projects-slider .single-project .seemore {
		background: #ea186f;
		color: #fff;
	}
	body.combo .projects-slider .single-project .seemore:hover {
		background: #;
	}
	body.combo .projects-slider .single-project .project-content h2 {
		color:#d68ba9;
	}

	body.complementos form.busca input[type="submit"] {
		background: #fabc09;
		border-color: #fabc09;
	}
	body.complementos .page-header-section .header-title span {
		color:#fabc09;
	}
	body.complementos .projects-filter-nav li.btn-active a {
		color: #fff;
		background: #fabc09;
	}
	
	body.complementos .projects-slider .single-project .seemore {
		background: #fabc09;
		color: #fff;
	}
	body.complementos .projects-slider .single-project .seemore:hover {
		background: #;
	}
	body.complementos .projects-slider .single-project .project-content h2 {
		color:#ead498;
	}
	
	body.estante form.busca input[type="submit"] {
		background: #ef2e32;
		border-color: #ef2e32;
	}
	body.estante .page-header-section .header-title span {
		color:#ef2e32;
	}
	body.estante .projects-filter-nav li.btn-active a {
		color: #fff;
		background: #ef2e32;
	}
	
	body.estante .projects-slider .single-project .seemore {
		background: #ef2e32;
		color: #fff;
	}
	body.estante .projects-slider .single-project .seemore:hover {
		background: #;
	}
	body.estante .projects-slider .single-project .project-content h2 {
		color:#ea8f90;
	}
</style>

</head>


<?php if ($cat == 'Caemmun Office') { ?><body class="office"><?php } ?>
<?php if ($cat == 'Caemmun Star') { ?><body class="star"><?php } ?>
<?php if ($cat == 'Caemmun Rooms') { ?><body class="rooms"><?php } ?>
<?php /* if ($cat == 'Caemmun') { ?><body class="Caemmun"><?php } */?>
<?php if ($sub == 'Todos') { ?><body class="Caemmun"><?php } ?>
<?php if ($sub == 'Bancada, TV Stand, Mesa para TV, Banc Tv') { ?><body class="bancada"><?php } ?>
<?php if ($sub == 'Combo, Combo, Combo, Combo') { ?><body class="combo"><?php } ?>
<?php if ($sub == 'Complementos, Complement, Complementos, Complénts') { ?><body class="complementos"><?php } ?>
<?php if ($sub == 'Estante, Entertainment Center, Centro de Entretenimiento, Étagère') { ?><body class="estante"><?php } ?>
<?php if ($sub == 'Home Theater, Entertainment Center, Centro de Entretenimiento, Centre de divertissement') { ?><body class="estante"><?php } ?>
<?php if ($sub == 'Painel, Panel, Panel, Panneau') { ?><body class="Caemmun"><?php } ?>
<?php if ($sub == 'Rack, TV Stand, Mesa para TV, Banc Tv') { ?><body class="bancada"><?php } ?>



	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>

	<!-- Projects section start -->
	<section class="projects-section lista pb50">
		<div class="container-site row">
			<div class="col-lg-3 col-12 d-flex justify-content-center">
				<?php if ($idioma == 'pt') { ?>
					<form class="busca">
						<input type="text" name="busca" placeholder="Sua pesquisa..." required>
						<input type="submit" value="Pesquisar">
					</form>
				<?php } elseif ($idioma == 'ing') { ?>
					<form class="busca">
						<input type="text" name="busca" placeholder="Your search..." required>
						<input type="submit" value="Search">
					</form>
				<?php } elseif ($idioma == 'esp') { ?>
					<form class="busca">
						<input type="text" name="busca" placeholder="Tu búsqueda..." required>
						<input type="submit" value="Buscar">
					</form>
				<?php } elseif ($idioma == 'fra') { ?>
					<form class="busca">
						<input type="text" name="busca" placeholder="Votre recherche..." required>
						<input type="submit" value="Chercher">
					</form>
				<?php } ?>
			</div>
			<?php if (isset($_GET['busca'])) {
			} else { ?>
				<ul class="projects-filter-nav col-lg-9 col-12">
					<li class="<?= $sub == 'Todos' || $sub == 'All' || $sub == 'Todas' ? 'btn-active' : '' ?>" data-filter="*">
						<a href="produtos?cat=<?= $_GET['cat'] ?>&sub=Todos" title="">
							<?php
							if ($idioma == 'pt') {
								echo 'Todos';
							} else if ($idioma == 'ing') {
								echo 'All';
							} else if ($idioma == 'esp') {
								echo 'Todas';
							} else if ($idioma == 'fra') {
								echo 'Tous';
							}
							?>
						</a>
					</li>
					<?php
					if ($idioma == 'pt') {
						$categoria = mysqli_query($connect, "SELECT cat_id, cat_titulo FROM tb_categorias INNER JOIN tab_produtos ON (tb_categorias.cat_id = tab_produtos.pro_categoria) WHERE cat_mae = '$categ_id' AND cat_titulo LIKE '%,%' AND pro_status = 'A' GROUP BY cat_id ORDER BY cat_titulo");
					} elseif ($idioma == 'ing') {
						$categoria = mysqli_query($connect, "SELECT cat_id, cat_titulo FROM tb_categorias INNER JOIN tab_produtos ON (tb_categorias.cat_id = tab_produtos.pro_categoria) WHERE cat_mae = '$categ_id' AND cat_titulo LIKE '%,%' AND pro_status = 'A' AND pro_titulo_ing != '' GROUP BY cat_id ORDER BY cat_titulo");
					} elseif ($idioma == 'esp') {
						$categoria = mysqli_query($connect, "SELECT cat_id, cat_titulo FROM tb_categorias INNER JOIN tab_produtos ON (tb_categorias.cat_id = tab_produtos.pro_categoria) WHERE cat_mae = '$categ_id' AND cat_titulo LIKE '%,%' AND pro_status = 'A' AND pro_titulo_esp != '' GROUP BY cat_id ORDER BY cat_titulo");
					} elseif ($idioma == 'fra') {
						$categoria = mysqli_query($connect, "SELECT cat_id, cat_titulo FROM tb_categorias INNER JOIN tab_produtos ON (tb_categorias.cat_id = tab_produtos.pro_categoria) WHERE cat_mae = '$categ_id' AND cat_titulo LIKE '%,%' AND pro_status = 'A' AND pro_titulo_fra != '' GROUP BY cat_id ORDER BY cat_titulo");
					}
					$arr_titulo['titulo'] = [];
					$arr_titulo['cat'] = [];
					$arr_titulo['dup'] = [];
					while ($categ = mysqli_fetch_assoc($categoria)) {
						$explode = explode(', ', $categ['cat_titulo']);
						$sub_pt = $explode[0];
						$sub_ing = $explode[1];
						$sub_esp = $explode[2];
						$sub_fra = $explode[3];

						switch ($idioma) {
							case 'pt':
								if (in_array($sub_pt, $arr_titulo['titulo'])) {
									$arr_titulo['dup'][$sub_pt] = $categ['cat_titulo'];
								}
								else {
									$arr_titulo['titulo'][] = $sub_pt;
									$arr_titulo['cat'][] = [
										'titulo' => $sub_pt,
										'cat_titulo' => $categ['cat_titulo']
									];
								}
							break;

							case 'ing':
								if (in_array($sub_ing, $arr_titulo['titulo'])) {
									$arr_titulo['dup'][$sub_ing] = $categ['cat_titulo'];
								}
								else {
									$arr_titulo['titulo'][] = $sub_ing;
									$arr_titulo['cat'][] = [
										'titulo' => $sub_ing,
										'cat_titulo' => $categ['cat_titulo']
									];
								}
							break;

							case 'esp':
								if (in_array($sub_esp, $arr_titulo['titulo'])) {
									$arr_titulo['dup'][$sub_esp] = $categ['cat_titulo'];
								}
								else {
									$arr_titulo['titulo'][] = $sub_esp;
									$arr_titulo['cat'][] = [
										'titulo' => $sub_esp,
										'cat_titulo' => $categ['cat_titulo']
									];
								}
							break;

							case 'fra':
								if (in_array($sub_fra, $arr_titulo['titulo'])) {
									$arr_titulo['dup'][$sub_fra] = $categ['cat_titulo'];
								}
								else {
									$arr_titulo['titulo'][] = $sub_fra;
									$arr_titulo['cat'][] = [
										'titulo' => $sub_fra,
										'cat_titulo' => $categ['cat_titulo']
									];
								}
							break;

							default: break;
						}
					}
					?>
						<? foreach ($arr_titulo['cat'] as $arr): ?>
							<?php if ($idioma == 'pt') {  ?>
								<li class="<?= $sub == $arr['cat_titulo'] ? 'btn-active' : '' ?>"><a href="produtos?cat=<?= $_GET['cat'] ?>&sub=<?= urlencode($arr['cat_titulo']) ?>&sub2=<?= urlencode($arr_titulo['dup'][$arr['titulo']]) ?>" title="<?= $arr['titulo'] ?>"><?= $arr['titulo'] ?></a></li>
							<?php } elseif ($idioma == 'ing') { ?>
								<li class="<?= $sub == $arr['cat_titulo'] ? 'btn-active' : '' ?>"><a href="produtos?cat=<?= $_GET['cat'] ?>&sub=<?= urlencode($arr['cat_titulo']) ?>&sub2=<?= urlencode($arr_titulo['dup'][$arr['titulo']]) ?>" title="<?= $arr['titulo'] ?>"><?= $arr['titulo'] ?></a></li>
							<?php } elseif ($idioma == 'esp') { ?>
								<li class="<?= $sub == $arr['cat_titulo'] ? 'btn-active' : '' ?>"><a href="produtos?cat=<?= $_GET['cat'] ?>&sub=<?= urlencode($arr['cat_titulo']) ?>&sub2=<?= urlencode($arr_titulo['dup'][$arr['titulo']]) ?>" title="<?= $arr['titulo'] ?>"><?= $arr['titulo'] ?></a></li>
							<?php } elseif ($idioma == 'fra') { ?>
								<li class="<?= $sub == $arr['cat_titulo'] ? 'btn-active' : '' ?>"><a href="produtos?cat=<?= $_GET['cat'] ?>&sub=<?= urlencode($arr['cat_titulo']) ?>&sub2=<?= urlencode($arr_titulo['dup'][$arr['titulo']]) ?>" title="<?= $arr['titulo'] ?>"><?= $arr['titulo'] ?></a></li>
							<?php } ?>
						<? endforeach; ?>
					<?php // die(var_dump($arr_titulo));
					?>
				</ul>
			<?php } ?>

			<div class="row projects-slider">
				<?php

				if ($sub == 'Todos' || $sub == 'All' || $sub == 'Todas') {
					$subcategorias = mysqli_query($connect, "SELECT cat_id FROM tb_categorias WHERE cat_mae = $categoria_id");
					$subs = mysqli_fetch_array($subcategorias);
					$in = '(' . implode(',', $subs) . ')';
					print_r($subs);

					$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' AND cat_mae = $categ_id AND cat_modulo = 'produtos' GROUP BY pro_id ORDER BY pro_titulo ASC");
				} else {
					$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' AND (cat_titulo = '$sub' OR cat_titulo = '$sub_2') GROUP BY pro_id ORDER BY pro_titulo ASC");
				}

				if ($busca != '') {
					$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_fotos as fot ON (pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' AND pro_titulo LIKE '%$busca%' GROUP BY pro_id ORDER BY pro_titulo ASC");
				}

				$tam_pro = mysqli_num_rows($produtos);
				if ($idioma == 'pt') {
					if ($tam_pro == 0) { ?>
						<div class="nd">
							<h1>Nenhum produto foi encontrado!</h1>
						</div>
						<div class="nd_b"><a href="#" onclick="window.history.back()">Voltar</a></div>
					<?php
					}
				} elseif ($idioma == 'ing') {
					if ($tam_pro == 0) { ?>
						<div class="nd">
							<h1>No products were found!</h1>
						</div>
						<div class="nd_b"><a href="home" onclick="window.history.back()">Back</a></div>
					<?php
					}
				} elseif ($idioma == 'esp') {
					if ($tam_pro == 0) { ?>
						<div class="nd">
							<h1>No se encontraron productos!</h1>
						</div>
						<div class="nd_b"><a href="home" onclick="window.history.back()">Voltar</a></div>
					<?php
					}
				} elseif ($idioma == 'fra') {
					if ($tam_pro == 0) { ?>
						<div class="nd">
							<h1>Aucun produit n'a été trouvé!</h1>
						</div>
						<div class="nd_b"><a href="home" onclick="window.history.back()">Voltar</a></div>
					<?php
					}
				}
				while ($produto = mysqli_fetch_assoc($produtos)) {

					?>

					<!--PORTUGUÊS-->
					<?php if ($idioma == 'pt') { ?>
						<?php if ($produto['pro_titulo'] != '') { ?>
							<div class="col-lg-3 col-md-12 box_sub">
								<div class="single-project project_sub set-bg pro_<?= $produto['pro_categoria'] ?>" style="background-size: cover; background-position:center center;" data-setbg="<?= PATH_PRODUTOS ?>/<?= $produto['pro_id'] ?>/original/<?= $produto['fot_titulo'] ?>">
									<div class="pro">
										<h2><?= $produto['pro_titulo'] ?></h2>
									</div>
									<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo']) ?>" class="btn-hover">
										<div class="project-content">
											<h2><?= $produto['pro_titulo'] ?></h2>
											<p></p>
											<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo']) ?>" title="Explorar produtos" class="seemore">Ver produto</a>
										</div>
									</a>
								</div>
							</div>
						<?php } ?>

						<!--INGLÊS-->
					<?php } elseif ($idioma == 'ing') { ?>
						<?php if ($produto['pro_titulo_ing'] != '') { ?>
							<div class="col-lg-3 col-md-12 box_sub">
								<div class="single-project project_sub set-bg pro_<?= $produto['pro_categoria'] ?>" style="background-size: cover; background-position:center center;" data-setbg="<?= PATH_PRODUTOS ?>/<?= $produto['pro_id'] ?>/original/<?= $produto['fot_titulo'] ?>">
									<div class="pro">
										<h2><?= $produto['pro_titulo_ing'] ?></h2>
									</div>
									<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo']) ?>" class="btn-hover">
										<div class="project-content">
											<h2><?= $produto['pro_titulo_ing'] ?></h2>
											<p></p>
											<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo_ing']) ?>" title="Explore products" class="seemore">See product</a>
										</div>
									</a>
								</div>
							</div>
						<?php } ?>
						<!--ESPANHOL-->
					<?php } elseif ($idioma == 'esp') { ?>
						<?php if ($produto['pro_titulo_esp'] != '') { ?>
							<div class="col-lg-3 col-md-12 box_sub">
								<div class="single-project project_sub set-bg pro_<?= $produto['pro_categoria'] ?>" style="background-size: cover; background-position:center center;" data-setbg="<?= PATH_PRODUTOS ?>/<?= $produto['pro_id'] ?>/original/<?= $produto['fot_titulo'] ?>">
									<div class="pro">
										<h2><?= $produto['pro_titulo_esp'] ?></h2>
									</div>
									<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo']) ?>" class="btn-hover">
										<div class="project-content">
											<h2><?= $produto['pro_titulo_esp'] ?></h2>
											<p></p>
											<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo_esp']) ?>" title="Explore los productos" class="seemore">Ver producto</a>
										</div>
									</a>
								</div>
							</div>
						<?php } ?>
					<?php } elseif ($idioma == 'fra') { ?>
						<?php if ($produto['pro_titulo_fra'] != '') { ?>
							<div class="col-lg-3 col-md-12 box_sub">
								<div class="single-project project_sub set-bg pro_<?= $produto['pro_categoria'] ?>" style="background-size: cover; background-position:center center;" data-setbg="<?= PATH_PRODUTOS ?>/<?= $produto['pro_id'] ?>/original/<?= $produto['fot_titulo'] ?>">
									<div class="pro">
										<h2><?= $produto['pro_titulo_fra'] ?></h2>
									</div>
									<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo']) ?>" class="btn-hover">
										<div class="project-content">
											<h2><?= $produto['pro_titulo_fra'] ?></h2>
											<p></p>
											<a href="produto?id=<?= $produto['pro_id'] ?>&pro=<?= urlencode($produto['pro_titulo_fra']) ?>" title="Découvrir les produits" class="seemore">Voir le produit</a>
										</div>
									</a>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</div>
		</div>

	</section>


	<?php include 'includes/footer_trabalhe.php'; ?>
	<?php include 'includes/scripts.php'; ?>
</body>

</html>