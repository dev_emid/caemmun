<?php
    // include_once __DIR__ . '/../src/IPInfo/Api.php';
    // try {
    //     $api = new Api('5c74d335f95c9e69d352a5925cbf2af81e75fef33db8a73eb360c8269bf866b4');

    //     function getUserIpAddr() {
    //         if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    //             $ip = $_SERVER['HTTP_CLIENT_IP'];
    //         }
    //         else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    //             $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    //         }
    //         else {
    //             $ip = $_SERVER['REMOTE_ADDR'];
    //         }

    //         return $ip;
    //     }

    //     $country_code = $api->getCountry(getUserIpAddr());
    // }

    // Propriedades padrões dos sites
    $protocolo = 'http';
    if(isset($_SERVER['HTTPS'])) 
    {
        if ($_SERVER['HTTPS'] == "on") {
            $protocolo = 'https';
        }
    }
    //IDIOMA
    if ($_COOKIE["idioma"] != '') {
        $idioma = $_COOKIE["idioma"];
    }else{
        $idioma = 'pt';
    }


    define("URL_EMIDCMS", "https://cms.emidgroup.com.br/caemmun");
    define("PATH_BANNERS", URL_EMIDCMS."/arquivos/banners/imagens/");
    define("PATH_PRODUTOS", URL_EMIDCMS."/arquivos/produtos/imagens/");
    define("PATH_CORES", URL_EMIDCMS."/arquivos/cores/imagens/");
    
    define("URL_BASE_SITE", "https://caemmun.com.br/site");

    define("HOST", "smtp.skymail.net.br");
    define("PORT", "587");
    define("USER", "no-reply@emid.com.br");
    define("PASSWORD", "senha@EMID21");
    define("FROM_NAME", "Caemmun Movelaria");
    define("FROM_EMAIL", "no-reply@emid.com.br");
    
    //URL UTILIZADA PRA ENVIAR REQUISICOES AJAX COM JAVASCRIPT
    define("URL_BASE_ENVIO_AJAX", 'request/action.php');
    
    
    $_url_base          = $protocolo . "://" . $_SERVER['HTTP_HOST'] . "/";
    $_url_arquivos      = $protocolo . ":cms.emidgroup.com.br/caemmun/arquivos";
    
    $_path_upload       = dirname(__DIR__);

    $script_filename    = explode('/', $_SERVER['SCRIPT_FILENAME']);
    $current_filename   = array_pop($script_filename);


    function limitarTexto($texto, $limite){
        $contador = strlen($texto);
        if ( $contador >= $limite ) {      
            $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
            return $texto;
        }
        else{
            return $texto;
        }
    } 


    // if (in_array($country_code['countryCode'], ['BR', 'PT'])) {
    //     $idioma = 'pt';
    // }
?>
