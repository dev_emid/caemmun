<?php

    include_once '../data/setup.php';   
    include_once '../data/config.php';       
    
    require_once '../src/PHPMailer/src/PHPMailer.php';
    require_once '../src/PHPMailer/src/SMTP.php';
    require_once '../src/PHPMailer/src/Exception.php';
    $action_by_user = $_POST['action_by_user'];    
           
    function is_image_or_pdf($image_type)
    {
        if(in_array($image_type ,  array("image/png", "image/jpeg", "image/jpg", "application/pdf")))
        {
            return true;
        }

        return false;
    }               
    
    $ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

    foreach ($_POST as $key => $value)
    {
        $_POST[$key] = utf8_decode($value);
    }       
    
    switch ($action_by_user)
    {           
        case 'contato':
        $nome       = isset($_POST['nome']) ? utf8_encode($_POST['nome']) : null; 
        $email      = isset($_POST['email']) ? utf8_encode($_POST['email']) : null;   
        $telefone   = isset($_POST['telefone']) ? utf8_encode($_POST['telefone']) : null; 
        $lang       = isset($_POST['lang']) ? utf8_encode($_POST['lang']) : null;                
        $message    = isset($_POST['mensagem']) ? utf8_encode($_POST['mensagem']) : null; 
        $check      = isset($_POST['check']) ? utf8_encode($_POST['check']) : null; 


                //EMAIL PARA Caemmun
                $email_send = new Email();

                $email_send->addMessage(
                   "Contato - Caemmun", 
                   "Olá, alguém solicitou um novo contato dentro do site! <br>
                    <h1>Dados de Contato</h1>  <br>
                    <strong>Nome:</strong>  $nome  <br>
                    <strong>Telefone:</strong>  $telefone  <br>
                    <strong>E-mail:</strong>  $email  <br>
                    <strong>Mensagem:</strong>  $message  <br>",
                    "Site - Caemmun", "sac@caemmun.com.br"
                )->send("vendas01@vendascaemmun.com.br");


                if(!$email_send->error())
                {
                    $retorno['status'] = 'success';
                }
                else
                {                            
                    $retorno['status'] = 'error';
                    $retorno['error'] = $email_send->error()->getMessage();
                } 
                           
        $sql_grava = "INSERT INTO tab_contato (con_nome, con_email, con_telefone, con_idioma, con_mensagem, con_check, con_status)   VALUES ('" . addslashes($nome) . "', '" . addslashes($email) . "', '" . addslashes($telefone) . "', '" . addslashes($lang) . "', '" . addslashes($message) . "', '" . addslashes($check) . "', 'I')";

        mysqli_query($connect, $sql_grava);
                
                if($connect->error)
                {
                    $retorno = ['status' => 'error', 'erro' => $connect->error];                   
                }
                else
                {
                    $last_id = $connect->insert_id;
                    
                    $retorno = ['status' => 'success'];
                }                
                
                if($retorno['status'] == 'success')
                {
                    //EMAIL PARA O CLIENTE
                    $email_send = new Email();

                    $email_send->addMessage(
                       "Contato - Caemmun", "Olá, vimos que você preencheu o formulário de contato dentro do nosso site, logo estaremos entrando em contato!", FROM_NAME, $email
                    )->send();


                    if(!$email_send->error())
                    {
                        $retorno['status'] = 'success';
                    }
                    else
                    {                            
                        $retorno['status'] = 'error';
                        $retorno['error'] = $email_send->error()->getMessage();
                    }
                }
                
                die(json_encode($retorno)); 
        break;

        case 'revendedor':
        $razao_social  = isset($_POST['razao_social']) ? utf8_encode($_POST['razao_social']) : null; 
        $nome_fantasia = isset($_POST['nome_fantasia']) ? utf8_encode($_POST['nome_fantasia']) : null; 
        $cnpj          = isset($_POST['cnpj']) ? utf8_encode($_POST['cnpj']) : null; 
        $inscricao     = isset($_POST['inscricao']) ? utf8_encode($_POST['inscricao']) : null;
        $nome          = isset($_POST['nome']) ? utf8_encode($_POST['nome']) : null; 
        $telefone      = isset($_POST['telefone']) ? utf8_encode($_POST['telefone']) : null; 
        $email         = isset($_POST['email']) ? utf8_encode($_POST['email']) : null; 
        $endereco      = isset($_POST['endereco']) ? utf8_encode($_POST['endereco']) : null; 
        $estado        = isset($_POST['estado']) ? utf8_encode($_POST['estado']) : null; 
        $cidade        = isset($_POST['cidade']) ? utf8_encode($_POST['cidade']) : null; 
        $mensagem      = isset($_POST['mensagem']) ? utf8_encode($_POST['mensagem']) : null; 
        $lang          = isset($_POST['lang']) ? utf8_encode($_POST['lang']) : null; 
        $check         = isset($_POST['check']) ? utf8_encode($_POST['check']) : null; 

            //EMAIL PARA Caemmun
                 $email_send = new Email();

                $email_send->addMessage(
                   "Solicitação de Revendedor - Caemmun", 
                   "Olá, alguém solicitou um novo contato dentro do site! <br>
                    <h1>Dados de Contato</h1>  <br>
                    <strong>Razão Social:</strong>  $razao_social  <br>
                    <strong>Nome Fantasia:</strong>  $nome_fantasia  <br>
                    <strong>CNPJ:</strong>  $cnpj  <br>
                    <strong>Inscrição Estadual:</strong>  $inscricao  <br>
                    <strong>Nome:</strong>  $nome  <br>
                    <strong>Telefone:</strong>  $telefone  <br>
                    <strong>E-mail:</strong>  $email  <br>
                    <strong>Endereço:</strong>  $endereco  <br>
                    <strong>Estado:</strong>  $estado  <br>
                    <strong>Cidade:</strong>  $cidade  <br>
                    <strong>Mensagem:</strong>  $mensagem  <br>",
                    "Solicitação de Revendedor - Caemmun", "lucas.souza@caemmun.com.br"
                )->send();


                if(!$email_send->error())
                {
                    $retorno['status'] = 'success';
                }
                else
                {                            
                    $retorno['status'] = 'error';
                    $retorno['error'] = $email_send->error()->getMessage();
                } 
                           
        $sql_grava = "INSERT INTO tab_revendedor (rev_razao_social, rev_nome_fantasia, rev_cnpj, rev_inscricao, rev_nome, rev_telefone, rev_email, rev_endereco, rev_estado, rev_cidade, rev_mensagem, rev_check, rev_lang, rev_status)   VALUES ('" . addslashes($razao_social) . "', '" . addslashes($nome_fantasia) . "', '" . addslashes($cnpj) . "', '" . addslashes($inscricao) . "', '" . addslashes($nome) . "', '" . addslashes($telefone) . "', '" . addslashes($email) . "', '" . addslashes($endereco) . "', '" . addslashes($estado) . "', '" . addslashes($cidade) . "', '" . addslashes($mensagem) . "', '" . addslashes($check) . "', '" . addslashes($lang) . "', 'I')";

        mysqli_query($connect, $sql_grava);
                
                if($connect->error)
                {
                    $retorno = ['status' => 'error', 'erro' => $connect->error];                   
                }
                else
                {
                    $last_id = $connect->insert_id;
                    
                    $retorno = ['status' => 'success'];
                }                
                
                if($retorno['status'] == 'success')
                {
                    //EMAIL PARA O CLIENTE
                    $email_send = new Email();

                    $email_send->addMessage(
                       "Revendedor - Caemmun", "Olá, vimos que você preencheu o formulário de revendedores dentro do nosso site, logo estaremos entrando em contato!", FROM_NAME, $email
                    )->send();


                    if(!$email_send->error())
                    {
                        $retorno['status'] = 'success';
                    }
                    else
                    {                            
                        $retorno['status'] = 'error';
                        $retorno['error'] = $email_send->error()->getMessage();
                    }
                }
                
                die(json_encode($retorno)); 
        break;

        /*case 'orcamento':

        $nome             = isset($_POST['nome']) ? $_POST['nome'] : null; 
        $telefone         = isset($_POST['telefone']) ? $_POST['telefone'] : null;
        $email            = isset($_POST['email']) ? $_POST['email'] : null; 
        $cidade           = isset($_POST['cidade']) ? $_POST['cidade'] : null; 
        $evento           = isset($_POST['evento']) ? $_POST['evento'] : null;                
        $data             = isset($_POST['data']) ? $_POST['data'] : null; 
        $cidade_evento    = isset($_POST['cidade-evento']) ? $_POST['cidade-evento'] : null; 
        $servico          = isset($_POST['servico']) ? $_POST['servico'] : null; 
        $termo            = isset($_POST['termo']) ? $_POST['termo'] : null;




                 //EMAIL PARA Caemmun
                $email_send = new Email();

                $email_send->addMessage(
                   "Solicitação de Orçamento - Caemmun", 
                   "Olá, alguém solicitou um novo orçamento dentro do site! <br>
                    <h1>Dados de Contato</h1>  <br>
                    <strong>Nome:</strong>  $nome  <br>
                    <strong>Telefone:</strong>  $telefone  <br>
                    <strong>E-mail:</strong>  $email  <br>
                    <strong>Cidade:</strong>  $cidade  <br>
                    <strong>Evento:</strong>  $evento  <br>
                    <strong>Cidade do Evento:</strong>  $cidade_evento <br>
                    <strong>Serviço Desejado:</strong>  $servico  <br>",
                    "Site - Caemmun", "giovane.flores@emid.com.br"
                )->send();


                if(!$email_send->error())
                {
                    $retorno['status'] = 'success';
                }
                else
                {                            
                    $retorno['status'] = 'error';
                    $retorno['error'] = $email_send->error()->getMessage();
                } 
                           
        $sql_grava = "INSERT INTO tab_soliciteorcamento (sol_nome, sol_telefone, sol_email, sol_cidade, sol_evento, sol_cidade_evento, sol_servico, sol_termo, sol_status)   VALUES ('" . addslashes($nome) . "', '" . addslashes($telefone) . "', '" . addslashes($email) . "', '" . addslashes($cidade) . "', '" . addslashes($evento) . "', '" . addslashes($cidade_evento) . "', '" . addslashes($servico) . "','SIM', 'I')";

        mysqli_query($connect, $sql_grava);
                
                if($connect->error)
                {
                    $retorno = ['status' => 'error', 'erro' => $connect->error];                   
                }
                else
                {
                    $last_id = $connect->insert_id;
                    
                    $retorno = ['status' => 'success'];
                }                
                
                if($retorno['status'] == 'success')
                {
                    //EMAIL PARA O CLIENTE
                    $email_send = new Email();

                    $email_send->addMessage(
                       "Contato - Caemmun", "Olá, vimos que você preencheu o formulário de Orçamento dentro do nosso site, logo estaremos entrando em contato!", FROM_NAME, $email
                    )->send();


                    if(!$email_send->error())
                    {
                        $retorno['status'] = 'success';
                    }
                    else
                    {                            
                        $retorno['status'] = 'error';
                        $retorno['error'] = $email_send->error()->getMessage();
                    }
                }
                
                die(json_encode($retorno)); 
        break;*/

        case 'curriculo':
                $nome       = isset($_POST['nome']) ? utf8_encode($_POST['nome']) : null; 
                $email      = isset($_POST['email']) ? utf8_encode($_POST['email']) : null; 
                $telefone   = isset($_POST['telefone']) ? utf8_encode($_POST['telefone']) : null; 
                $mensagem   = isset($_POST['mensagem']) ? utf8_encode($_POST['mensagem']) : null;
                $check      = isset($_POST['check']) ? utf8_encode($_POST['check']) : null;  
   
                $errors = array();                                                                                                     
                $curriculo = $_FILES['curriculo']; //tirar
                
                $name_photo = $curriculo['name'];                
                $arquivos = $_FILES['curriculo'];

                $sql_grava = "INSERT INTO tab_trabalhe (tra_nome, tra_telefone, tra_email, tra_mensagem, tra_check, tra_status) VALUES ('" . addslashes($nome) ."', '" . addslashes($telefone) ."', '" . addslashes($email) ."', '" . addslashes($mensagem) ."', '" . addslashes($check) ."', 'I')";
                
                mysqli_query($connect, $sql_grava);  
                if($connect->error)
                {
                    $retorno = ['status' => 'error', 'erro' => $connect->error];                 
                }
                else
                {

                    $id = $connect->insert_id;
                    
                    $retorno = ['status' => 'success'];
                    
                    // Tamanho máximo do arquivo em bytes 
                    $tamanho = 2000000; 

                    foreach($arquivos as $key => $imagem)
                    {         
                        // Verifica se o arquivo é uma imagem
                        if($key == 'type')
                        {
                            if(!is_image_or_pdf($imagem))
                            {
                                $errors[] = "Isso não é uma imagem.";
                            }
                        }
                        // Verifica se o tamanho da imagem é maior que o tamanho permitido
                        else if($key == 'size')
                        {
                            if((int)$imagem[$key] > $tamanho)
                            {
                                $errors[] = "A imagem pode ter no máximo ".$tamanho." bytes";
                            }
                        }                                                          
                    }     
                    if (count($errors) == 0) 
                    {
                        foreach($arquivos as $key => $imagem)
                        {       
                            
                            if($key == 'name')
                            {
                                $nome_imagem = '';
                                $ext = [];

                                // Pega extensão da imagem
                                preg_match("/\.(gif|bmp|png|jpg|jpeg|pdf){1}$/i", $imagem, $ext);

                                // Gera um nome único para a imagem
                                $nome_imagem = md5(uniqid(time())) . "." . $ext[1];  

                                // Caminho de onde ficará a imagem
                                $caminho_imagem = "../arquivos/trabalhe/" .$id. '/' . $nome_imagem;
                            

                                $path = "../arquivos/trabalhe/". $id; 

                                if(!file_exists($path))
                                {
                                    mkdir($path);                                                                        
                                }                

                                if(file_exists($path))
                                {
                                    if(move_uploaded_file($arquivos['tmp_name'], $caminho_imagem))
                                    {
                                        $urlFile = $nome_imagem;

                                        $sql_foto = "UPDATE tab_trabalhe SET curriculo = 'https://caemmun.com.br/site/arquivos/trabalhe/".$id.'/'.utf8_encode($nome_imagem)."' WHERE tra_id = {$id}";       
                                        

                                        if($connect->query($sql_foto))
                                        {
                                            $retorno = ['status' => 'success'];
                                        }
                                        else 
                                        {
                                            $retorno = ['status' => 'error', 'error' => 'Erro na hora de gravar a foto no Banco de Dados'];

                                            die(json_encode($retorno));
                                        }                        
                                    }
                                    else
                                    {
                                        $retorno = ['status' => 'error', 'error' => 'Erro na hora de enviar a imagem para o servidor!'];

                                        die(json_encode($retorno));
                                    }
                                }
                                else
                                {
                                    $retorno = ['status' => 'error', 'error' => 'Diretório ainda não foi criado...'];

                                    die(json_encode($retorno));                                    
                                }
                            }                        
                        }
                    }            
                    else
                    {     
                        $retorno = ['status' => 'error', 'error' => 'Erro de tamanho de foto ou o tipo.'];                                   
                    }                     
                }
                    $email_send = new Email();

                    $email_send->addMessage(
                       "Trabalhe Conosco - Caemmun", "Olá, vimos que você preencheu nosso formulário e enviou seu currículo dentro do nosso site, logo estaremos entrando em contato!", $nome, $email
                    )->send();


                    if(!$email_send->error())
                    {
                        $retorno['status'] = 'success';
                    }
                    else
                    {                            
                        $retorno['status'] = 'error';
                        $retorno['error'] = $email_send->error()->getMessage();
                    }      


                  $email = new Email();

                    $email->addMessage(
                    "Trabalhe Conosco - Caemmun",
                    "Olá, alguém enviou um novo currículo dentro do site!<br>
                    Acesse o painel para visualizar: <a href='https://cms.emidgroup.com.br/caemmun' target='_blank'>Clique Aqui</a><br>
                    Acesso rápido: <a href='https://caemmun.com.br/site/arquivos/trabalhe/$id/$nome_imagem'>Clique aqui</a>
                    ",
                    "Trabalhe Conosco - Caemmun", "rh1@caemmun.com.br"
                    )->send("vendas01@vendascaemmun.com.br");


                    if(!$email->error())
                    {
                        $retorno['status'] = 'success';
                    }
                    else
                    {                           
                        $retorno['status'] = 'error';
                        $retorno['error'] = $email->error()->getMessage();
                    }                        
                      
                
                die(json_encode($retorno)); 
   
        break;                

    }
    
    
    class Email
    {
        /** @var PHPMailer */
        private $mail;

        /** @var stdClass */
        private $data;

        /** @var Exception */
        private $error;    


        public function __construct()
        {
            $this->mail = new PHPMailer\PHPMailer\PHPMailer(TRUE);
            $this->data = new stdClass();

            $this->mail->isSMTP();
            $this->mail->isHTML();
            $this->mail->setLanguage("br");

            $this->mail->SMTPAuth = true; 
            //$this->mail->SMTPDebug = 1; 
            $this->mail->SMTPSecure = '';
            $this->mail->CharSet = 'utf-8'; 

            $this->mail->Host = HOST;
            $this->mail->Port = PORT;
            $this->mail->Username = USER;
            $this->mail->Password = PASSWORD;                
        }

        public function addMessage($subject, $body, $recipient_name, $recipient_email)
        {
            $this->data->subject = $subject;
            $this->data->body = $body;
            $this->data->recipient_name = $recipient_name;
            $this->data->recipient_email = $recipient_email;

            return $this;        
        }

        public function attach(string $filePath, string $fileName)
        {
            $this->data->attach[$filePath] = $fileName;
        }

        public function send($from_name = FROM_NAME, $from_email = FROM_EMAIL)
        {
            try {
                $this->mail->Subject = $this->data->subject;
                $this->mail->msgHTML($this->data->body);
                $this->mail->addAddress($this->data->recipient_email, $this->data->recipient_name);
                $this->mail->setFrom($from_email, $from_name);


                if(!empty($this->data->attach))
                {
                    foreach($this->data->attach as $path => $name)
                    {
                        $this->mail->addAttachment($path, $name);
                    }
                }

                $this->mail->send();

                return true;

            } 
            catch (Exception $ex) {
                $this->error = $ex;

                return false;
            }
        }

        public function error()
        {
            return $this->error;
        }   
    }    

    