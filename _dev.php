<?php
	include 'data/config.php';
	include 'data/setup.php';

	$cat = urldecode($_GET['cat']);

	if ($cat == 'Caemmun') { $categ_id = 1; }elseif($cat == 'Caemmun Star'){ $categ_id = 3; }elseif($cat == 'Caemmun Office'){ $categ_id = 4; }elseif($cat == 'Caemmun Masterchef'){ $categ_id = 5; }

	$cat_titulo = mysqli_query($connect, "SELECT cat_titulo FROM tb_categorias WHERE cat_id = '$categ_id'");

	while ($tit = mysqli_fetch_assoc($cat_titulo)) {
		$titulo = $tit['cat_titulo']; 
	} 
     
    $sub = urldecode($_GET['sub']); 

    if(isset($_GET['busca'])){
		$busca = $_GET['busca'];
		$titulo = $_GET['busca'];
		$sub = $_GET['busca'];
	}else{
		$busca = '';
	}	
?>

<!DOCTYPE html>
<html>
<head>
	<?php
        include 'includes/head.php';

        $redirect = null;
        if ($idioma == 'pt' && ($sub == 'All' || $sub == 'Todas')) {
            $redirect = URL_BASE_SITE.'/produtos?cat='.$cat.'&sub=Todos';
        }
        else if ($idioma == 'ing' && ($sub == 'Todos' || $sub == 'Todas')) {
            $redirect = URL_BASE_SITE.'/produtos?cat='.$cat.'&sub=All';
        }
        else if ($idioma == 'esp' && ($sub == 'All' || $sub == 'Todos')) {
            $redirect = URL_BASE_SITE.'/produtos?cat='.$cat.'&sub=Todas';
        }
    
        if (!is_null($redirect)) {
            echo '<meta http-equiv="refresh" content="0;url='.$redirect.'">';
        }
    ?>
</head>
<body>
	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>

	<!-- Projects section start -->
	<section class="projects-section lista pb50">
		<div class="container-site row">
			<div class="col-lg-3 col-12 d-flex justify-content-center">
				<?php if ($idioma == 'pt'){ ?>
					<form class="busca">
						<input type="text" name="busca" placeholder="Sua pesquisa..." required>
						<input type="submit" value="Pesquisar">
					</form>
				<?php }elseif ($idioma == 'ing'){ ?>
					<form class="busca">
						<input type="text" name="busca" placeholder="Your search..." required>
						<input type="submit" value="Search">
					</form>
				<?php }elseif ($idioma == 'esp'){ ?>
					<form class="busca">
						<input type="text" name="busca" placeholder="Tu búsqueda..." required>
						<input type="submit" value="Buscar">
					</form>
				<?php } ?>
			</div>
			<?php if(isset($_GET['busca'])){}else{?>
			<ul class="projects-filter-nav col-lg-9 col-12">
				<li class="<?= $sub == 'Todos' || $sub == 'All' || $sub == 'Todas' ? 'btn-active' : '' ?>" data-filter="*">
                    <a href="produtos?cat=<?=$_GET['cat']?>&sub=Todos" title="">
                        <?php
                            if ($idioma == 'pt') {
                                echo 'Todos';
                            }
                            else if ($idioma == 'ing') {
                                echo 'All';
                            }
                            else if ($idioma == 'esp') {
                                echo 'Todas';
                            }
                        ?>
                    </a>
                </li>
				<?php 
				if ($idioma == 'pt'){ 
					$categoria = mysqli_query($connect, "SELECT cat_id, cat_titulo FROM tb_categorias INNER JOIN tab_produtos ON (tb_categorias.cat_id = tab_produtos.pro_categoria) WHERE cat_mae = '$categ_id' AND cat_titulo LIKE '%,%' AND pro_status = 'A' GROUP BY cat_id ORDER BY cat_titulo");
				}elseif ($idioma == 'ing'){
					$categoria = mysqli_query($connect, "SELECT cat_id, cat_titulo FROM tb_categorias INNER JOIN tab_produtos ON (tb_categorias.cat_id = tab_produtos.pro_categoria) WHERE cat_mae = '$categ_id' AND cat_titulo LIKE '%,%' AND pro_status = 'A' AND pro_titulo_ing != '' GROUP BY cat_id ORDER BY cat_titulo");
				}elseif ($idioma == 'esp'){
					$categoria = mysqli_query($connect, "SELECT cat_id, cat_titulo FROM tb_categorias INNER JOIN tab_produtos ON (tb_categorias.cat_id = tab_produtos.pro_categoria) WHERE cat_mae = '$categ_id' AND cat_titulo LIKE '%,%' AND pro_status = 'A' AND pro_titulo_esp != '' GROUP BY cat_id ORDER BY cat_titulo");
				}	
					while ($categ = mysqli_fetch_assoc($categoria)) {
						$explode = explode(', ', $categ['cat_titulo']);
					    $sub_pt = $explode[0];
					    $sub_ing = $explode[1];
					    $sub_esp = $explode[2];
				?>
					<?php if ($idioma == 'pt'){  ?>
						<li class="<?= $sub == $categ['cat_titulo'] ? 'btn-active' : '' ?>"><a href="produtos?cat=<?=$_GET['cat']?>&sub=<?=urlencode($categ['cat_titulo'])?>" title="<?=$sub_pt?>"><?=$sub_pt?></a></li>
					<?php }elseif ($idioma == 'ing'){ ?>
						<li class="<?= $sub == $categ['cat_titulo'] ? 'btn-active' : '' ?>"><a href="produtos?cat=<?=$_GET['cat']?>&sub=<?=urlencode($categ['cat_titulo'])?>" title="<?=$sub_ing?>"><?=$sub_ing?></a></li>
					<?php }elseif ($idioma == 'esp'){ ?>
						<li class="<?= $sub == $categ['cat_titulo'] ? 'btn-active' : '' ?>"><a href="produtos?cat=<?=$_GET['cat']?>&sub=<?=urlencode($categ['cat_titulo'])?>" title="<?=$sub_esp?>"><?=$sub_esp?></a></li>
					<?php } ?>
				<?php } ?>
			</ul>
			<?php } ?>

			<div class="row projects-slider">
				<?php 

					if ($sub == 'Todos' || $sub == 'All' || $sub == 'Todas') {
						$subcategorias = mysqli_query($connect, "SELECT cat_id FROM tb_categorias WHERE cat_mae = $categoria_id");
						$subs = mysqli_fetch_array($subcategorias);
						$in = '(' . implode(',', $subs) .')';
						print_r($subs);

						$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' AND cat_mae = $categ_id AND cat_modulo = 'produtos' GROUP BY pro_id ORDER BY pro_titulo ASC");
					}else{
						$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' AND cat_titulo = '$sub' GROUP BY pro_id ORDER BY pro_titulo ASC");
					}

					if($busca != ''){
						$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_fotos as fot ON (pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' AND pro_titulo LIKE '%$busca%' GROUP BY pro_id ORDER BY pro_titulo ASC");
					}

					$tam_pro = mysqli_num_rows($produtos);
					if ($idioma == 'pt'){
						if($tam_pro == 0){ ?>
							<div class="nd"><h1>Nenhum produto foi encontrado!</h1></div>
							<div class="nd_b"><a href="#" onclick="window.history.back()">Voltar</a></div>
					<?php
						}
					}elseif ($idioma == 'ing'){
						if($tam_pro == 0){ ?>
							<div class="nd"><h1>No products were found!</h1></div>
							<div class="nd_b"><a href="home" onclick="window.history.back()">Back</a></div>
					<?php
						}
					}elseif ($idioma == 'esp'){
						if($tam_pro == 0){ ?>
							<div class="nd"><h1>No se encontraron productos!</h1></div>
							<div class="nd_b"><a href="home" onclick="window.history.back()">Voltar</a></div>
					<?php
						}
					}
					while ($produto = mysqli_fetch_assoc($produtos)) {

				?>

						<!--PORTUGUÊS-->
						<?php if ($idioma == 'pt'){ ?>
							<?php if ($produto['pro_titulo'] != ''){ ?>
								<div class="col-lg-3 col-md-12 box_sub">
									<div class="single-project project_sub set-bg pro_<?=$produto['pro_categoria']?>" style="background-size: cover; background-position:center center;" data-setbg="<?=PATH_PRODUTOS?>/<?=$produto['pro_id']?>/original/<?=$produto['fot_titulo']?>">
										<div class="pro">
											<h2><?= $produto['pro_titulo'] ?></h2>
										</div>
										<a href="produto?id=<?=$produto['pro_id']?>&pro=<?=urlencode($produto['pro_titulo'])?>" class="btn-hover">
											<div class="project-content">
												<h2><?= $produto['pro_titulo'] ?></h2>
												<p></p>
												<a href="produto?id=<?=$produto['pro_id']?>&pro=<?=urlencode($produto['pro_titulo'])?>" title="Explorar produtos" class="seemore">Ver produto</a>
											</div>
										</a>
									</div>
								</div>
							<?php } ?>
						
						<!--INGLÊS-->	
						<?php }elseif ($idioma == 'ing'){ ?>
						<?php if ($produto['pro_titulo_ing'] != ''){ ?>
						<div class="col-lg-3 col-md-12 box_sub">	
							<div class="single-project project_sub set-bg pro_<?=$produto['pro_categoria']?>" style="background-size: cover; background-position:center center;" data-setbg="<?=PATH_PRODUTOS?>/<?=$produto['pro_id']?>/original/<?=$produto['fot_titulo']?>">
								<div class="pro">
									<h2><?= $produto['pro_titulo_ing'] ?></h2>
								</div>
							<a href="produto?id=<?=$produto['pro_id']?>&pro=<?=urlencode($produto['pro_titulo'])?>" class="btn-hover">	
								<div class="project-content">
									<h2><?= $produto['pro_titulo_ing'] ?></h2>
									<p></p>
									<a href="produto?id=<?=$produto['pro_id']?>&pro=<?=urlencode($produto['pro_titulo_ing'])?>" title="Explore products" class="seemore">See product</a>
								</div>
							</a>
							</div>
						</div>	
						<?php } ?>						
						<!--ESPANHOL-->	
						<?php }elseif ($idioma == 'esp'){ ?>
						<?php if ($produto['pro_titulo_esp'] != ''){ ?>
						<div class="col-lg-3 col-md-12 box_sub">	
							<div class="single-project project_sub set-bg pro_<?=$produto['pro_categoria']?>" style="background-size: cover; background-position:center center;" data-setbg="<?=PATH_PRODUTOS?>/<?=$produto['pro_id']?>/original/<?=$produto['fot_titulo']?>">
								<div class="pro">
									<h2><?= $produto['pro_titulo_esp'] ?></h2>
								</div>
							<a href="produto?id=<?=$produto['pro_id']?>&pro=<?=urlencode($produto['pro_titulo'])?>" class="btn-hover">	
								<div class="project-content">
									<h2><?= $produto['pro_titulo_esp'] ?></h2>
									<p></p>
									<a href="produto?id=<?=$produto['pro_id']?>&pro=<?=urlencode($produto['pro_titulo_esp'])?>" title="Explore los productos" class="seemore">Ver producto</a>
								</div>
							</a>
							</div>	
						</div>						
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
		
	</section>
	

	<?php include 'includes/footer_trabalhe.php'; ?>
	<?php include 'includes/scripts.php'; ?>
</body>
</html>