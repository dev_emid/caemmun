<?php
	include 'data/config.php';
	include 'data/setup.php';

	if ($idioma == 'pt'){ 
		$titulo = 'Suporte ao produto';
	}elseif ($idioma == 'ing'){ 
		$titulo = 'Product support';
	}elseif ($idioma == 'esp'){
		$titulo = 'Soporte de producto';
	}elseif ($idioma == 'fra'){
		$titulo = 'Support produit';
	}  
	if (isset($_GET['busca']) && $idioma == 'pt') {
		$busca = "AND pro_titulo LIKE '%" . $_GET['busca'] . "%'";
	}elseif(isset($_GET['busca']) && $idioma == 'ing'){
		$busca = "AND pro_titulo_ing LIKE '%" . $_GET['busca'] . "%'";
	}elseif(isset($_GET['busca']) && $idioma == 'esp'){
		$busca = "AND pro_titulo_esp LIKE '%" . $_GET['busca'] . "%'";
	}elseif(isset($_GET['busca']) && $idioma == 'fra'){
		$busca = "AND pro_titulo_fra LIKE '%" . $_GET['busca'] . "%'";
	}else{
		$busca = '';
	}
	$search = $_GET['busca'];
?>
	 
<!DOCTYPE html>
<html>
<head>
	<?php include 'includes/head.php'; ?>
</head>
<style type="text/css">
		.autoc {
		width: 80%;	
		margin: 0 auto;
		position: relative;
		}
		.input-group {
		position: relative;
		}
		label {
		position: absolute;
		right: 2%;
		top: 50%;
		transform: translatey(-50%);
		color: rgba(0, 0, 0, 0.08);
		transition: all 0.2s ease;
		}
		input {
		width: 100%;
		padding: 15px 30px 15px 12px;
		border: 2px solid rgba(0, 0, 0, 0.08);
		outline: none;
		font-size: 16px;
		box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.06);
		color: #1c4d86;
		font-weight: bold;
		letter-spacing: 1px;
		border-radius: 1px;
		transition: all 0.2s ease;
		}
		input:focus {
		border-color: #1c4d86;
		}
		input:focus + label {
		transform: scale(1.05) translatey(-50%);
		color: #1c4d86;
		}
		#apps {
		margin-top: 80px;
		}
		.app {
		display: inline-block;
		text-align: center;
		border: 1px solid rgba(255, 255, 255, 0.1);
		border-radius: 3px;
		transition: all 0.2s ease;
		}
		.app:hover {
		border-color: rgba(255, 255, 255, 0.5);
		}
		.app i {
		font-size: 2.4em;
		color: transparent;
		}
		.app p {
		font-size: 36px;
		margin-top: 6px;
		transition: 0.2s all ease;
		color: #fff;
		font-family: 'GothamSSmBlack', sans-serif;
		position: relative;
		z-index: 1;
		line-height: 46px;
		margin-top: 60px;
		}
		.app:hover p {
		color: rgba(255, 255, 255, 0.8);
		}
		.suggestion-list {
		background-color: #fff;
		padding: 18px 24px 6px 12px;
		border-radius: 0 0 6px 6px;
		position: absolute;
		width: 100%;
		margin-top: 0px;
		border: 2px solid #1c4d86;
		border-top: none;
		z-index: 8;
		}
		.suggestion-list.hidden {
		display: none;
		}
		.suggestion-list p {
		margin-bottom: 12px;
		cursor: pointer;
		z-index: 9;
		text-transform: capitalize;
		color: black;
    	font-family: 'GothamSSmMedium';
    	padding: 5px 0;
		}
		.suggestion-list p:hover{
		background: #12c3f426;	
		}
		#search{
			text-transform: capitalize;
		}
		.suggestion-list i {
		margin-right: 12px;
		color: #1c4d86;
		}
		.autocomplete{
			padding: 80px 0;
		}
		.search_title h1{
		text-align: center;
		color: #1c4d86;
		margin-bottom: 15px;
		}
		.search_title h3{
		text-align: center;
		font-size: 24px;
		font-family: 'GothamSSmMedium';
		}
		.busca{
		position: absolute;
		width: 130px;
		text-align: center;
		padding: 15px 20px;
		color: white;
		background: #1c4d86;
		right: 0;
		box-shadow: 0px 2px 5px rgb(0 0 0 / 6%);
		}
		.app{
		width: 100%;
		height: 440px;
		margin-bottom: 30px;
		position: relative;
		}
		.app:after {
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    content: "";
	    background: #00000094;
	    top: 0;
	    left: 0;
		}
		.botoes_catalogo{
		width: calc(100% - 30px);
		display: flex;
		position: absolute;
		bottom: 60px;
		z-index: 1;
		margin: 0 15px;
		}
		.botoes_catalogo a{
		width: 48%;
		display: flex;
		color: white;
		font-family: 'GothamSSmMedium';
		padding: 12px;
		font-size: 14px;
		align-items: center;
		justify-content: center;
		margin: 0 10px;
		background: #1c4d86;
		border-radius: 8px;
		}
		.botoes_catalogo a:hover{
		text-decoration: underline;
		}
		.botoes_catalogo img{
	    width: 30px;
	    margin-right: 10px;
		}
		@media(max-width: 800px){
			.busca{
				margin: 0;
			}
		}
		@media(max-width: 600){
			.autoc{
				width: 95%!important;
			}
		}
		@media(max-width: 380px){
			.botoes_catalogo img {
			    width: 22px;
			    margin-right: 10px;
			}
			.botoes_catalogo a{
				font-size: 10px;
			}			
		}
</style>
<body>
	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>
		<!--PORTUGUÊS-->
		<?php if ($idioma == 'pt'){ ?>

			<section class="autocomplete">
				<div class="container">
					<div class="search_title">
						<h1>Suporte ao Produto</h1>
						<h3>Obtenha os manuais e as fichas técnicas</h3>
					</div>
					<form class="autoc">
						<div class="input-group">
							<input type="text" id="search" name="busca" placeholder="Pesquisa..." autocomplete="off" required>
							<input type="submit" class="busca" value="Buscar">
						</div>
						<div class="suggestion-list hidden">
						</div>
					</form>
					<div id="apps" class="row">
						<?php 
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' {$busca} AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != '' AND pro_titulo != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
						<div class="col-lg-6">
							<div class="app" style="background: url('<?=PATH_PRODUTOS?>/<?=$produto['pro_id']?>/original/<?=$produto['fot_titulo']?>'); background-size: cover; background-repeat: no-repeat;">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo']?></p>
								<div class="botoes_catalogo">
									<a href="<?=$produto['pro_catalogo']?>" target="_blank"><img src="dev/img/icon/pdf.svg"> Manual de Montagem</a>
									<a href="<?=$produto['pro_info']?>" target="_blank"><img src="dev/img/icon/guide.svg">Informações Técnicas</a>
								</div>
							</div>
						</div>
						
						<?php } ?>

						<?php
						if (isset($_GET['busca'])) {
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_titulo NOT LIKE '%{$search}%' AND pro_status = 'A' AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != ''  AND pro_titulo != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
							<div class="app d-none">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo']?></p>
							</div>						
						<?php } } ?>
					</div>
				</div>
			</section>

		<!--INGLÊS-->	
		<?php }elseif ($idioma == 'ing'){ ?>
			
			<section class="autocomplete">
				<div class="container">
					<div class="search_title">
						<h1>Product Support</h1>
						<h3>Get the manuals and data sheets</h3>
					</div>
					<form class="autoc">
						<div class="input-group">
							<input type="text" id="search" name="busca" placeholder="Search..." autocomplete="off" required>
							<input type="submit" class="busca" value="Search">
						</div>
						<div class="suggestion-list hidden">
						</div>
					</form>
					<div id="apps" class="row">
						<?php 
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' {$busca} AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != '' AND pro_titulo_ing != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
						<div class="col-lg-6">
							<div class="app" style="background: url('<?=PATH_PRODUTOS?>/<?=$produto['pro_id']?>/original/<?=$produto['fot_titulo']?>'); background-size: cover; background-repeat: no-repeat;">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo_ing']?></p>
								<div class="botoes_catalogo">
									<a href="<?=$produto['pro_catalogo']?>" target="_blank"><img src="dev/img/icon/pdf.svg"> Assembly Instructions</a>
									<a href="<?=$produto['pro_info']?>" target="_blank"><img src="dev/img/icon/guide.svg"> Technical Information</a>
								</div>
							</div>
						</div>
						
						<?php } ?>

						<?php
						if (isset($_GET['busca'])) {
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_titulo NOT LIKE '%{$search}%' AND pro_status = 'A' AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != '' AND pro_titulo_ing != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
							<div class="app d-none">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo_ing']?></p>
							</div>						
						<?php } } ?>
					</div>
				</div>
			</section>

		<!--ESPANHOL-->	
		<?php }elseif ($idioma == 'esp'){ ?>
			
			<section class="autocomplete">
				<div class="container">
					<div class="search_title">
						<h1>Soporte de producto</h1>
						<h3>Obtenga los manuales y las hojas de datos</h3>
					</div>
					<form class="autoc">
						<div class="input-group">
							<input type="text" id="search" name="busca" placeholder="Buscar..." autocomplete="off" required>
							<input type="submit" class="busca" value="Buscar">
						</div>
						<div class="suggestion-list hidden">
						</div>
					</form>
					<div id="apps" class="row">
						<?php 
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' {$busca} AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != '' AND pro_titulo_esp != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
						<div class="col-lg-6">
							<div class="app" style="background: url('<?=PATH_PRODUTOS?>/<?=$produto['pro_id']?>/original/<?=$produto['fot_titulo']?>'); background-size: cover; background-repeat: no-repeat;">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo_esp']?></p>
								<div class="botoes_catalogo">
									<a href="<?=$produto['pro_catalogo']?>" target="_blank"><img src="dev/img/icon/pdf.svg"> Instrucciones de Montaje</a>
									<a href="<?=$produto['pro_info']?>" target="_blank"><img src="dev/img/icon/guide.svg"> Información Técnica</a>
								</div>
							</div>
						</div>
						
						<?php } ?>

						<?php
						if (isset($_GET['busca'])) {
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_titulo NOT LIKE '%{$search}%' AND pro_status = 'A' AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != '' AND pro_titulo_esp != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
							<div class="app d-none">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo_esp']?></p>
							</div>						
						<?php } } ?>
					</div>
				</div>
			</section>

		<?php }elseif ($idioma == 'fra'){ ?>
			
			<section class="autocomplete">
				<div class="container">
					<div class="search_title">
						<h1>Support produit</h1>
						<h3>Obtenez des manuels et des fiches techniques</h3>
					</div>
					<form class="autoc">
						<div class="input-group">
							<input type="text" id="search" name="busca" placeholder="Chercher..." autocomplete="off" required>
							<input type="submit" class="busca" value="Chercher">
						</div>
						<div class="suggestion-list hidden">
						</div>
					</form>
					<div id="apps" class="row">
						<?php 
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_status = 'A' {$busca} AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != '' AND pro_titulo_fra != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
						<div class="col-lg-6">
							<div class="app" style="background: url('<?=PATH_PRODUTOS?>/<?=$produto['pro_id']?>/original/<?=$produto['fot_titulo']?>'); background-size: cover; background-repeat: no-repeat;">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo_fra']?></p>
								<div class="botoes_catalogo">
									<a href="<?=$produto['pro_catalogo']?>" target="_blank"><img src="dev/img/icon/pdf.svg"> Instructions de montage</a>
									<a href="<?=$produto['pro_info']?>" target="_blank"><img src="dev/img/icon/guide.svg"> Information technique</a>
								</div>
							</div>
						</div>
						
						<?php } ?>

						<?php
						if (isset($_GET['busca'])) {
							$produtos = mysqli_query($connect, "SELECT * FROM tab_produtos as pro INNER JOIN tb_categorias as cat INNER JOIN tb_fotos as fot ON (pro.pro_categoria = cat.cat_id AND pro.pro_id = fot.fot_vinculo) WHERE fot_modulo = 'produtos' AND fot_capa = '1' AND pro_titulo NOT LIKE '%{$search}%' AND pro_status = 'A' AND cat_modulo = 'produtos' AND pro_catalogo != '' AND pro_info != '' AND pro_titulo_fra != '' GROUP BY pro_id ORDER BY pro_titulo ASC");
							while ($produto = mysqli_fetch_assoc($produtos)) {
						?>
							<div class="app d-none">								
								<i class="fab fa-adobe"></i>
								<p class="produto"><?= $produto['pro_titulo_fra']?></p>
							</div>						
						<?php } } ?>
					</div>
				</div>
			</section>

		<?php } ?>

		<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

		
		<script type="text/javascript">
			var all_apps = document.querySelectorAll('.app');
			var search = document.querySelector('#search');
			var listContainer = document.querySelector('.suggestion-list');
			

			var app_list = [];

			for (let i = 0; i < all_apps.length; i++) {
			  let app_title = all_apps[i].querySelector('p').innerText.toLowerCase();
			  let app_icon = all_apps[i].querySelector('i').classList.value;

			  let obj = {};
			  obj.app_title = app_title;
			  obj.app_icon = app_icon;

			  app_list.push(obj);
			}

			search.addEventListener('keyup', generateAppList);	
			//search.addEventListener('blur', hideAppList);			


			function generateAppList(event) {
			  var fragment = document.createDocumentFragment();

			  var userInput = event.target.value.toLowerCase();

			  if (userInput.length === 0) {
			    listContainer.classList.add('hidden');
			    return false;
			  }

			  listContainer.innerHTML = '';
			  listContainer.classList.remove('hidden');

			  var filteredList = app_list.filter(function (arr) {
			    return arr.app_title.includes(userInput);
			  });

				if (filteredList.length === 0) {
				    let paragraph = document.createElement('p');
				    	paragraph.innerText = 'Nenhum produto encontrado!';
				    
				    fragment.appendChild(paragraph);
				}

			  

			  else {
			    for (let i = 0; i < filteredList.length; i++) {
			      let paragraph = document.createElement('p');			      
			      let icon = document.createElement('i');
			      let span = document.createElement('span');			      	

			      icon.classList.value = filteredList[i].app_icon;
			      span.innerText = filteredList[i].app_title;
			      paragraph.appendChild(icon);
			      paragraph.appendChild(span);
			      
			      paragraph.setAttribute('onclick', 'changeValue(this)');

			      fragment.appendChild(paragraph);

			      
			    }
			  }

			  listContainer.appendChild(fragment);			  	
			}

			function hideAppList() {
			  	listContainer.classList.add('hidden');
			}

			function changeValue(o){				
				var cae = o.innerHTML.split("<span>", 2);	
				var cae2 = cae[1].split("</span>", 2);
		    	document.getElementById('search').value=cae2[0];
		    	listContainer.classList.add('hidden');
		    }	



		</script>	

	<?php include 'includes/footer.php'; ?>
	<?php include 'includes/scripts.php'; ?>
</body>
</html>