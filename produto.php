<?php
	include 'data/config.php';
	include 'data/setup.php';
	$pagina = 'produto';
	$pro = urldecode($_GET['pro']);
	$id = $_GET['id'];

	$cat_titulo = mysqli_query($connect, "SELECT cat_titulo, cat_mae FROM tb_categorias INNER JOIN tab_produtos ON(tab_produtos.pro_categoria = tb_categorias.cat_id) WHERE pro_id = '$id'");
	
	while ($tit = mysqli_fetch_assoc($cat_titulo)) {
		$explode = explode(', ', $tit['cat_titulo']);
		$sub_pt = $explode[0];
		$sub_ing = $explode[1];
		$sub_esp = $explode[2];
		$sub_fra = $explode[3];

		$mae = $tit['cat_mae'];
		$cat_mae = mysqli_query($connect, "SELECT cat_titulo FROM tb_categorias WHERE cat_id = $mae");

		if ($m = mysqli_fetch_assoc($cat_mae)) {
			if ($idioma == 'pt'){  
				$titulo = $m['cat_titulo'] . ' <br>  <span class="bread">' . $sub_pt . '</span>';
			 }elseif ($idioma == 'ing'){ 
				$titulo = $m['cat_titulo'] . ' <br>  <span class="bread">' . $sub_ing . '</span>';
			 }elseif ($idioma == 'esp'){ 
				$titulo = $m['cat_titulo'] . ' <br>  <span class="bread">' . $sub_esp . '</span>';
			 }elseif ($idioma == 'fra'){ 
				$titulo = $m['cat_titulo'] . ' <br>  <span class="bread">' . $sub_fra . '</span>';
			 } 
			
		}
	}  
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'includes/head.php'; ?>
</head>
<style>
	/*.owl-carousel .owl-nav .owl-prev, .owl-carousel .owl-nav .owl-next, .owl-carousel .owl-dot{
	display: inline-flex;
		width: 50px;
	    height: 50px;
	    justify-content: center;
	    align-items: center;
	    background: linear-gradient(45deg, #1c4d86, #547293ab);
	    color: white;
	    margin: 0px;
	}
	.owl-carousel .owl-nav .owl-prev:hover, .owl-carousel .owl-nav .owl-next:hover, .owl-carousel .owl-dot:hover{
		background: #1c4d86;
	}*/
	.owl-prev{
		position: absolute;
    	left: -100px;
    	width: 60px;
	}
	.owl-next{
		position: absolute;
    	right: -100px;
    	width: 60px;
	}
	.owl-nav{
		display: flex;
	    justify-content: space-between;
	}
	.owl-controls{
		top: calc(50% - 25px);
	}
	@media screen and (max-width: 1340px){
		.owl-prev, .owl-next{
			position: unset;
		}
	}
	@media screen and (max-width: 767px){
		.owl-carousel .owl-nav .owl-prev, .owl-carousel .owl-nav .owl-next, .owl-carousel .owl-dot{
			width: 40px;
			height: 40px;
		}
		.owl-controls{
			top: calc(50% - 20px);
		}
	}
	@media only screen and (max-width: 480px){
		.page-header-section {
	    	height: 300px;
		}
	}
</style>
<body class="">
	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>

	<!-- Intro section start -->
	<section class="intro-section spad bg-f9">
		<?php 

		$produto = mysqli_query($connect, "SELECT * FROM tab_produtos WHERE pro_id = '$id' AND pro_status = 'A'"); 

		while ($prod = mysqli_fetch_assoc($produto)) {
			$cores = $prod['pro_cores'];
		?>
		<div class="container">
			<div class="row mb100">
				<div class="col-lg-12 intro-text">
					<h1><span><?=$pro?></span></h1>
				</div>
				<div class="col-lg-12">
					<div class="service-slider">
						<?php 
							$img_produtos = mysqli_query($connect, "SELECT fot_titulo FROM tb_fotos WHERE fot_vinculo = '$id' AND fot_modulo = 'produtos' ORDER BY fot_capa DESC, fot_ordem ASC");  
							while ($fot = mysqli_fetch_assoc($img_produtos)) { ?>
								<div class="ss-single">
									<img src="<?=PATH_PRODUTOS?><?=$id?>/original/<?=$fot['fot_titulo']?>" alt="<?=$pro?>">
								</div>
						<?php } ?>
					</div>
				</div>
			</div>

		</div>

		<div class="container">

			<div class="row mb100 intro-produto">

				<div class="col-md-12 desc_prod">
					<div class="row">
						<div class="col-md-1 align-self-center"><img class="ico" src="dev/img/icon/pantone.svg" alt="Cores dos Produtos"></div>
						<div class="col-md-11">
							<!--PORTUGUÊS-->
							<?php if ($idioma == 'pt'){ ?>
								<h2>Cores disponíveis</h2>
							<!--INGLÊS-->	
							<?php }elseif ($idioma == 'ing'){ ?>
								<h2>Colors available</h2>				
							<!--ESPANHOL-->	
							<?php }elseif ($idioma == 'esp'){ ?>
								<h2>Colores disponibles</h2>						
							<?php }elseif ($idioma == 'fra'){ ?>
								<h2>Couleurs disponibles</h2>						
							<?php } ?>
							<div class="row">
								<?php 
									$cores_produtos = mysqli_query($connect, "SELECT cor_titulo, cor_id, fot_titulo FROM tab_cores LEFT JOIN tb_fotos ON (tab_cores.cor_id = tb_fotos.fot_vinculo) WHERE cor_id IN ($cores) AND fot_modulo = 'cores' AND cor_status = 'A' ORDER BY cor_ordem ASC"); 

									while ($cor = mysqli_fetch_assoc($cores_produtos)) {
								?>
									<figure class="col-lg-2 col-md-4 col-sm-6 col-6">
										<img src="<?=PATH_CORES?><?=$cor['cor_id']?>/original/<?=$cor['fot_titulo']?>" alt="<?=$cor['cor_titulo']?>">
										<figcaption><?=$cor['cor_titulo']?></figcaption>
									</figure>
								<?php } ?>
							</div>					
						</div>
					</div>
				</div>

				<div class="col-md-12 desc_prod">
					<div class="row">

						<div class="col-md-1 align-self-center"><img class="ico" src="dev/img/icon/briefing.svg" alt="Descrição do Produto"></div>
						<div class="col-md-11">
							<!--PORTUGUÊS-->
							<?php if ($idioma == 'pt'){ ?>
								<h2>Descrição do produto</h2>
								<p class="text-justify">
									<?=$prod['pro_descricao']?>
								</p>
							<!--INGLÊS-->	
							<?php }elseif ($idioma == 'ing'){ ?>
								<h2>Product description</h2>
								<p class="text-justify">
									<?=$prod['pro_descricao_ing']?>
								</p>				
							<!--ESPANHOL-->	
							<?php }elseif ($idioma == 'esp'){ ?>
								<h2>Descripción del producto</h2>
								<p class="text-justify">
									<?=$prod['pro_descricao_esp']?>
								</p>						
							<?php }elseif ($idioma == 'fra'){ ?>
								<h2>Description du produit</h2>
								<p class="text-justify">
									<?=$prod['pro_descricao_fra']?>
								</p>						
							<?php } ?>
						</div>

					</div>
				</div>

				<div class="col-md-12 desc_prod">
					<div class="row">

						<div class="col-md-1 align-self-center"><img class="ico" src="dev/img/icon/info.svg" alt="Dimensões do Produto"></div>
						<div class="col-md-11">
							<!--PORTUGUÊS-->
							<?php if ($idioma == 'pt'){ ?>
								<h2>Dimensões</h2>
								<p class="text-justify">
									Largura: <?=$prod['pro_largura']?> mm<br>
									Altura: <?=$prod['pro_altura']?> mm<br>
									Profundidade: <?=$prod['pro_profundidade']?> mm<br>
									Peso: <?=$prod['pro_peso']?> kg | Cubagem: <?=$prod['pro_cubagem']?> m³<br>
									<?php
										if($prod['pro_tv'] != ''){
									?>
									Espaço para TV: <?=$prod['pro_tv']?> Polegadas
									<?php
										}
									?>
								</p>
							<!--INGLÊS-->	
							<?php }elseif ($idioma == 'ing'){ ?>
								<h2>Dimensions</h2>
								<p class="text-justify">
									Width: <?=$prod['pro_largura_ing']?> in<br>
									Height: <?=$prod['pro_altura_ing']?> in<br>
									Depth: <?=$prod['pro_profundidade_ing']?> in<br>
									Weight: <?=$prod['pro_peso_ing']?> kg | Cubage: <?=$prod['pro_cubagem_ing']?> m³<br>
									<?php
										if($prod['pro_tv'] != ''){
									?>
									TV space: <?=$prod['pro_tv_ing']?> Inches
									<?php
										}
									?>
								</p>			
							<!--ESPANHOL-->	
							<?php }elseif ($idioma == 'esp'){ ?>
								<h2>Dimensiones</h2>
								<p class="text-justify">
									Anchura: <?=$prod['pro_largura_esp']?> mm<br>
									Altura: <?=$prod['pro_altura_esp']?> mm<br>
									Profundidad: <?=$prod['pro_profundidade_esp']?> mm<br>
									Peso: <?=$prod['pro_peso_esp']?> kg | Cubicación: <?=$prod['pro_cubagem_esp']?> m³<br>
									<?php
										if($prod['pro_tv'] != ''){
									?>
									Espacio de TV: <?=$prod['pro_tv_esp']?> Pulgadas
									<?php
										}
									?>
								</p>						
							<?php }elseif ($idioma == 'fra'){ ?>
								<h2>Dimensions</h2>
								<p class="text-justify">
									Largeur: <?=$prod['pro_largura_fra']?> mm<br>
									Hauteur: <?=$prod['pro_altura_fra']?> mm<br>
									Profondeur: <?=$prod['pro_profundidade_fra']?> mm<br>
									Poids: <?=$prod['pro_peso_fra']?> kg | Cubage: <?=$prod['pro_cubagem_fra']?> m³<br>
									<?php
										if($prod['pro_tv'] != ''){
									?>
									Espace télé: <?=$prod['pro_tv_fra']?> Pouces
									<?php
										}
									?>
								</p>						
							<?php } ?>
							
						</div>

					</div>
				</div>

				

				<div class="col-md-12 desc_prod">
					<div class="row">

						<div class="col-md-1 align-self-center"><img class="ico" src="dev/img/icon/file.svg" alt="Suporte para o produto"></div>
						<div class="col-md-11">
							<!--PORTUGUÊS-->
							<?php if ($idioma == 'pt'){ ?>
								<h2>Suporte para o produto</h2>
								<div class="links">
									<?php if ($prod['pro_catalogo'] != ''){ ?>
										<a href="<?=$prod['pro_catalogo']?>" title="Manual de Montagem" target="_blank" class="catalogo">Manual de Montagem</a>
									<?php } ?>
									<?php if ($prod['pro_imagens'] != ''){ ?>
										<a href="<?=$prod['pro_imagens']?>" title="Download de Imagens" target="_blank" class="image">Download de imagens</a>
									<?php } ?>
									<?php if ($prod['pro_info'] != ''){ ?>
										<a href="<?=$prod['pro_info']?>" title="Download de Informações Técnicas" target="_blank" class="info">Informações Técnicas</a>
									<?php } ?>
								</div>
							<!--INGLÊS-->	
							<?php }elseif ($idioma == 'ing'){ ?>
								<h2>Product support</h2>
								<div class="links">
									<?php if ($prod['pro_catalogo'] != ''){ ?>
										<a href="<?=$prod['pro_catalogo']?>" title="Assembly Instructions" target="_blank" class="catalogo">Assembly Instructions</a>
									<?php } ?>
									<?php if ($prod['pro_imagens'] != ''){ ?>
										<a href="<?=$prod['pro_imagens']?>" title="Download Images" target="_blank" class="image">Download Images</a>
									<?php } ?>
									<?php if ($prod['pro_info'] != ''){ ?>
										<a href="<?=$prod['pro_info']?>" title="Download Technical Information" target="_blank" class="info">Download Technical Information</a>
									<?php } ?>
								</div>				
							<!--ESPANHOL-->	
							<?php }elseif ($idioma == 'esp'){ ?>
								<h2>Suporte de producto</h2>
								<div class="links">
									<?php if ($prod['pro_catalogo'] != ''){ ?>
										<a href="<?=$prod['pro_catalogo']?>" title="Instrucciones de Montaje" target="_blank" class="catalogo">Instrucciones de Montaje</a>
									<?php } ?>
									<?php if ($prod['pro_imagens'] != ''){ ?>
										<a href="<?=$prod['pro_imagens']?>" title="Descargar imágenes" target="_blank" class="image">Descargar imágenes</a>
									<?php } ?>
									<?php if ($prod['pro_info'] != ''){ ?>
										<a href="<?=$prod['pro_info']?>" title="Descargar información técnica" target="_blank" class="info">Descargar información técnica</a>
									<?php } ?>
								</div>						
							<?php }elseif ($idioma == 'fra'){ ?>
								<h2>Support produit</h2>
								<div class="links">
									<?php if ($prod['pro_catalogo'] != ''){ ?>
										<a href="<?=$prod['pro_catalogo']?>" title="Instructions de montage" target="_blank" class="catalogo">Instructions de montage</a>
									<?php } ?>
									<?php if ($prod['pro_imagens'] != ''){ ?>
										<a href="<?=$prod['pro_imagens']?>" title="Télécharger des images" target="_blank" class="image">Télécharger des images</a>
									<?php } ?>
									<?php if ($prod['pro_info'] != ''){ ?>
										<a href="<?=$prod['pro_info']?>" title="Télécharger les informations techniques" target="_blank" class="info">Télécharger les informations techniques</a>
									<?php } ?>
								</div>						
							<?php } ?>
						</div>

					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</section>
	

	<?php include 'includes/footer_trabalhe.php'; ?>
	<?php include 'includes/scripts.php'; ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".owl-prev").html("<img src='dev/img/icon/left-arrow.svg'>");
			$(".owl-next").html("<img src='dev/img/icon/chevron.svg'>");
		});
	</script>
</body>
</html>