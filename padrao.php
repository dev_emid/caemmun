<?php
	include 'data/config.php';
	include 'data/setup.php';

	if ($idioma == 'pt'){ 
		$titulo = 'Contato';
	}elseif ($idioma == 'ing'){ 
		$titulo = 'Contact';
	}elseif ($idioma == 'esp'){
		$titulo = 'Contacto';
	}elseif ($idioma == 'fra'){
		$titulo = 'Contact';
	}  
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'includes/head.php'; ?>
</head>
<body>
	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>

	<!--PORTUGUÊS-->
		<section class="page-section pt100">

<div id="popup1" class="overlay">
	<div class="popup">
		<a class="close" href="#">&times;</a>
		<div class="content">
			<img src="dev/img/popupcaemmun.jpg" alt="aviso">
		</div>
	</div>
</div>
		</section>
		<style>

.button {
  font-size: 1em;
  padding: 10px;
  color: #fff;
  border: 2px solid #06D85F;
  border-radius: 20px/50px;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
}
.button:hover {
  background: #06D85F;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: #;
  opacity: 1;
  z-index: 999;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  .box{
    width: 70%;
  }
  .popup{
    width: 70%;
  }
}

.overlay.fechar {
  visibility: hidden;
  opacity: 0;
}
		</style>
	
		



	<?php include 'includes/footer.php'; ?>
	<?php include 'includes/scripts.php'; ?>
<script>
 $(function(){
		$(".close").on("click", function() {
      $(".overlay").addClass("fechar");
		});
  })
 
	
</script>

</body>
</html>