<?php
	include 'data/setup.php';
    include 'data/config.php';   
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>

	<?php include 'includes/head.php'; ?>
</head>

<body>
	<?php include 'includes/header.php'; ?>
	
<div id="popup1" class="overlay">
	<div class="popup">
		<a class="close" href="#">&times;</a>
		<div class="content">
			<img src="dev/img/popupcaemmun.jpg" alt="aviso">
		</div>
	</div>
</div>
		</section>
		<style>

.button {
  font-size: 1em;
  padding: 10px;
  color: #fff;
  border: 2px solid #06D85F;
  border-radius: 20px/50px;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
}
.button:hover {
  background: #06D85F;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: #;
  opacity: 1;
  z-index: 999;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  .box{
    width: 70%;
  }
  .popup{
    width: 70%;
  }
}

.overlay.fechar {
  visibility: hidden;
  opacity: 0;
}
		</style>
	
	
	
	<!-- Hero section start -->
	<section class="hero-section">
		<!-- left social link ber -->
		<div class="left-bar">
			<div class="left-bar-content">
				<div class="social-links">
					<a href="https://br.pinterest.com/caemmunmovelaria/_created/" title="Pinterest" rel="noreferrer" target="_blank"><i class="fa fa-pinterest"></i></a>
					<a href="https://www.youtube.com/channel/UCfJiKOIwwx-v6DeVE5mxLSQ" title="Youtube" rel="noreferrer" target="_blank"><i class="fa fa-youtube"></i></a>
					<a href="https://www.instagram.com/caemmunmovelaria/" title="Instagram" rel="noreferrer" target="_blank" ><i class="fa fa-instagram"></i></a>
					<a href="https://www.facebook.com/caemmun" title="Facebook" rel="noreferrer" target="_blank"><i class="fa fa-facebook"></i></a>
				</div>
			</div>
		</div>
		<!-- hero slider area -->
		<?php if ($idioma == 'pt') { 
			$ban = '';
		?>
			<div class="hero-slider ban-desk portuguese">
		<?php }elseif ($idioma == 'ing'){ 
			$ban = '_ing';
		?>
			<div class="hero-slider ban-desk ingles">
		<?php }elseif ($idioma == 'esp'){ 
			$ban = '_esp';
		?>
			<div class="hero-slider ban-desk espanhol">
		<?php }elseif ($idioma == 'fra'){ 
			$ban = '_fra';
		?>
			<div class="hero-slider ban-desk franca">
		<?php } ?>

			<?php 
				$banners = mysqli_query($connect, "SELECT * FROM tab_banners INNER JOIN tb_fotos ON (tb_fotos.fot_vinculo = tab_banners.id) WHERE fot_modulo = 'banners' AND status = 'A' AND inicio <= NOW() AND fim >= NOW() ORDER BY ordem ASC");

				while ($banner = mysqli_fetch_assoc($banners)) {
					$explode = explode(' ', $banner['titulo'.$ban]);
			?>
			<div class="hero-slide-item set-bg " style="background-position: center" data-setbg="<?=PATH_BANNERS?><?=$banner['id']?>/original/<?=$banner['fot_titulo']?>">
				<div class="slide-inner">
					<div class="slide-content">
						<h2><?=$explode[0]?><br><?=$explode[1] . ' ' . $explode[2]?></h2>
						<!--PORTUGUÊS-->
						<?php if ($idioma == 'pt' && $banner['link'] != ''){ ?>
							<a href="<?=$banner['link']?>" title="Conheça" class="site-btn sb-light">Conheça</a>
						<!--INGLÊS-->	
						<?php }elseif ($idioma == 'ing' && $banner['link_ing'] != ''){ ?>
							<a href="<?=$banner['link_ing']?>" title="Know More" class="site-btn sb-light">Know more</a>
						<!--ESPANHOL-->	
						<?php }elseif ($idioma == 'esp' && $banner['link_esp'] != ''){ ?>
							<a href="<?=$banner['link_esp']?>" title="Sepa mas" class="site-btn sb-light">Sepa mas</a>
						<?php }elseif ($idioma == 'fra' && $banner['link_fra'] != ''){ ?>
							<a href="<?=$banner['link_fra']?>" title="Rencontrer" class="site-btn sb-light">Rencontrer</a>
						<?php } ?>
						
					</div>	
				</div>
			</div>
			
			<?php } ?>	
		</div>
		<!-- hero slider area -->
		<?php if ($idioma == 'pt') { 
			$ban = '';
		?>
			<div class="hero-slider ban-mob portuguese">
		<?php }elseif ($idioma == 'ing'){ 
			$ban = '_ing';
		?>
			<div class="hero-slider ban-mob ingles">
		<?php }elseif ($idioma == 'esp'){ 
			$ban = '_esp';
		?>
			<div class="hero-slider ban-mob espanhol">
		<?php }elseif ($idioma == 'fra'){ 
			$ban = '_fra';
		?>
			<div class="hero-slider ban-mob franca">
		<?php } ?>

			<?php 
				$banners = mysqli_query($connect, "SELECT * FROM tab_banners INNER JOIN tb_fotos ON (tb_fotos.fot_vinculo = tab_banners.id) WHERE fot_modulo = 'banners' AND status = 'A' AND inicio <= NOW() AND fim >= NOW() ORDER BY ordem ASC");

				while ($banner = mysqli_fetch_assoc($banners)) {
					$explode = explode(' ', $banner['titulo'.$ban]);
			?>
			<div class="hero-slide-item set-bg " style="background-position: center" data-setbg="<?=PATH_BANNERS?><?=$banner['id']?>/original/<?=$banner['fot_titulo']?>">
				<div class="slide-inner">
						
				</div>
				<div class="slide-content">
					<h2><?=$explode[0]?><br><?=$explode[1] . ' ' . $explode[2]?></h2>
					<!--PORTUGUÊS-->
					<?php if ($idioma == 'pt' && $banner['link'] != ''){ ?>
						<a href="<?=$banner['link']?>" title="Conheça" class="site-btn sb-light">Conheça</a>
					<!--INGLÊS-->	
					<?php }elseif ($idioma == 'ing' && $banner['link_ing'] != ''){ ?>
						<a href="<?=$banner['link_ing']?>" title="Know More" class="site-btn sb-light">Know more</a>
					<!--ESPANHOL-->	
					<?php }elseif ($idioma == 'esp' && $banner['link_esp'] != ''){ ?>
						<a href="<?=$banner['link_esp']?>" title="Sepa mas" class="site-btn sb-light">Sepa mas</a>
					<?php } ?>
					
				</div>
			</div>
			
			<?php } ?>	
		</div>
		<div class="slide-num-holder" id="snh-1"></div>
		<!-- <div class="hero-right-text">Criando um novo futuro</div> -->
	</section>
	<!-- Hero section end -->

	<!-- CTA section start -->
	<section class="cta-section pt100 pb50">
		<div class="cta-image-box"></div>
		<div class="container">
			<div class="row">
				<div class="col-xl-8 col-lg-7 pl-lg-0 offset-lg-5 offset-xl-4 cta-content">
					<!--PORTUGUÊS-->



					<?php if ($idioma == 'pt'){ ?>
						<h2 class="sp-title">SOMOS ÚNICOS <br> SOMOS <span> CAEMMUN</span></h2>
							<h2>30 anos</h2>
								<p>Neste ano comemoramos 30 anos de história!
								História feita de pessoas, crescemos, expandimos, fizemos uma sucessão profissional, abrimos novas fábricas, tudo isso pelas mãos de nossos clientes, colaboradores, representantes e fornecedores. Construímos por onde passamos relações de confiança e parceria.
								Através dos anos nos tornamos referência no mercado moveleiro, oferecendo soluções inteligentes em mobiliário para todo território nacional e para mais de 60 países, temos como objetivo garantir a melhor experiência na compra de móveis para o lojista com produtos pensados e desenvolvidos para levar bem-estar aos consumidores através da qualidade e design de cada solução.
								Com uma estrutura fabril composta por 2 fábricas a Caemmun é completa, temos um mix de produtos com mais de 150 SKUS, nossa linha é composta por móveis para sala de estar com as marcas Caemmun Movelaria e Caemmun Star, escritório com a marca Caemmun Office e dormitório com a marca Caemmun Rooms.
								Somos especialistas no que fazemos de melhor, MÓVEIS funcionais e de qualidade!
								</p>
						
						
					<!--INGLÊS-->	
					<?php }elseif ($idioma == 'ing'){ ?>
						<h2 class="sp-title">WE ARE UNIQUE<br> WE ARE <span>CAEMMUN</span></h2>
						<h2>30 years</h2>
							<p>This year we celebrate 30 years of history! History made of people, we grew, expanded, made a professional succession, opened new factories, all this through the hands of our customers, employees, representatives and suppliers. Wherever we go, we build relationships of trust and partnership.
							Over the years we have become a reference in the furniture market, offering intelligent furniture solutions throughout the national territory and for more than 60 countries, we aim to guarantee the best experience in the purchase of furniture for the shopkeeper with products designed and developed to bring well-being. consumers through the quality and design of each solution.
							With a manufacturing structure composed of 2 factories, Caemmun is complete, we have a product mix with more than 150 SKUS, our line consists of furniture for living room with the brands Caemmun Movelaria and Caemmun Star, office with the brand Caemmun Office and dormitory branded by Caemmun Rooms.
							We are specialists in what we do best, functional and quality FURNITURE!
							</p>
							
					<?php }elseif ($idioma == 'esp'){ ?>
						<h2 class="sp-title">SOMOS UNICOS<br> SOMOS <span>CAEMMUN</span></h2>
						<h2>30 años</h2>
						<p>Cette année nous fêtons 30 ans d'histoire! Histoire faite de personnes, nous avons grandi, grandi, fait une relève professionnelle, ouvert de nouvelles usines, tout cela entre les mains de nos clients, employés, représentants et fournisseurs. Partout où nous allons, nous construisons des relations de confiance et de partenariat.
						Au fil des ans, nous sommes devenus une référence sur le marché du meuble, offrant des solutions de mobilier intelligent sur tout le territoire national et pour plus de 60 pays, nous visons à garantir la meilleure expérience dans l'achat de meubles pour le commerçant avec des produits conçus et développés pour apporter bien-être des consommateurs à travers la qualité et la conception de chaque solution.
						Avec une structure de fabrication composée de 2 usines, Caemmun est complet, nous avons un mix produit avec plus de 150 références, notre ligne se compose de mobilier de salon avec les marques Caemmun Movelaria et Caemmun Star, de bureau avec la marque Caemmun Office et de marque dortoir par Caemmun Rooms.
						Nous sommes spécialistes dans ce que nous faisons de mieux, des MEUBLES fonctionnels et de qualité!

						</p>

						
					<!-- FRANCÊS -->
					<?php }elseif ($idioma == 'fra'){ ?>
						<h2 class="sp-title">NOUS SOMMES UNIQUES,<br> NOUS SOMMES <span>CAEMMUN</span></h2>
						<h2>30 ans</h2>
							<p>Cette année nous fêtons 30 ans d'histoire ! Histoire faite de personnes, nous avons grandi, grandi, fait une relève professionnelle, ouvert de nouvelles usines, tout cela entre les mains de nos clients, employés, représentants et fournisseurs. Partout où nous allons, nous construisons des relations de confiance et de partenariat.
							Au fil des ans, nous sommes devenus une référence sur le marché du meuble, offrant des solutions intelligentes de mobilier sur tout le territoire national et pour plus de 60 pays, nous visons à garantir la meilleure expérience dans l'achat de meubles pour le commerçant avec des produits conçus et développés pour apporter du bien-être aux consommateurs à travers la qualité et la conception de chaque solution.
							Avec une structure de fabrication composée de 2 usines, Caemmun est complet, nous avons un mix produit avec plus de 150 références, notre ligne se compose de mobilier de salon avec les marques Caemmun Movelaria et Caemmun Star, de bureau avec la marque Caemmun Office et de marque dortoir par Caemmun Rooms.
							Nous sommes spécialistes dans ce que nous faisons de mieux, des MEUBLES fonctionnels et de qualité !
							</p>
						<div class="cta-icons">
							<a href="sobre" title="Savoir plus" class="site-btn sb-light">Savoir plus</a>
						</div>	
					<?php } ?>	
				</div>
			</div>
		</div>
	</section>
	<!-- CTA section end -->

	<!-- Projects section start -->
	<section class="projects-section pb50">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="section-title">
						<!--PORTUGUÊS-->
						<?php if ($idioma == 'pt'){ ?>
							<h1>Móveis do <span>seu</span> Jeito</h1>
							<p>Móveis que compõe ambientes com sentimentos e sensações, fazendo com que a funcionalidade, a beleza e a individualidade interajam em perfeita harmonia.</p>
						<!--INGLÊS-->	
						<?php }elseif ($idioma == 'ing'){ ?>
							<h1>Furniture <span>your</span> Way</h1>
							<p>Furniture that composes environments with feelings and sensations, making functionality, beauty and individuality interact in perfect harmony.</p>
						<!--ESPANHOL-->	
						<?php }elseif ($idioma == 'esp'){ ?>
							<h1>Muebles a <span>tu</span> Manera</h1>
							<p>Mobiliario que compone ambientes con sentimientos y sensaciones, haciendo que funcionalidad, belleza e individualidad interactúen en perfecta armonía.</p>
						<?php }elseif ($idioma == 'fra'){ ?>
							<h1>Meubles à <span>votre</span> Guise</h1>
							<p>Meubles qui vont apporter um ambiance ou les sentiments, les sensations et la commodité se réunissent en créant  une harmonie parfaite entre la beauté et la personnalité.</p>
						<?php } ?>
					</div>
				</div>

				<div class="col-lg-4 btnbox align-self-end">
					<!--PORTUGUÊS-->
					<?php if ($idioma == 'pt'){ ?>
						<a href="revendedor" title="Conheça" class="site-btn ">Seja revendedor</a>
					<!--INGLÊS-->	
					<?php }elseif ($idioma == 'ing'){ ?>
						<a href="revendedor" title="See more" class="site-btn ">Become a reseller</a>
					<!--ESPANHOL-->	
					<?php }elseif ($idioma == 'esp'){ ?>
						<a href="revendedor" title="Ver más" class="site-btn ">Sea Distribuidor</a>
					<?php }elseif ($idioma == 'fra'){ ?>
						<a href="revendedor" title="Rencontrer" class="site-btn ">Devenez Revendeur</a>
					<?php } ?>	
				</div>
				
			</div>
		</div>

		<div id="projects-carousel" class="projects-slider">

			<?php 

			$cat_0 = mysqli_query($connect, "SELECT cat_mae, pro_id, fot_titulo FROM tb_categorias INNER JOIN tab_produtos INNER JOIN tb_fotos ON(tab_produtos.pro_categoria = tb_categorias.cat_id) AND (tab_produtos.pro_id = tb_fotos.fot_vinculo) WHERE pro_status = 'A' AND fot_modulo = 'produtos' AND fot_capa = 1 AND pro_capa = 'S' GROUP BY cat_mae ORDER BY cat_mae ASC");

			while ($cat_head = mysqli_fetch_assoc($cat_0)) { 
				$mae = $cat_head['cat_mae'];

			$categor = mysqli_query($connect, "SELECT cat_titulo FROM tb_categorias WHERE cat_id = {$mae}");

				if($pro_cat = mysqli_fetch_assoc($categor)){

				?>
			
				<div class="single-project set-bg rest" style="background-size: cover;" data-setbg="<?=PATH_PRODUTOS?><?=$cat_head['pro_id']?>/original/<?=$cat_head['fot_titulo']?>">
					<div class="pro">
						<?php if ($idioma == 'pt'){ ?>
							<h2>Linha <?=$pro_cat['cat_titulo']?></h2>
						<!--INGLÊS-->	
						<?php }elseif ($idioma == 'ing'){ ?>
							<h2><?=$pro_cat['cat_titulo']?> Line</h2>
						<!--ESPANHOL-->	
						<?php }elseif ($idioma == 'esp'){ ?>
							<h2>Línea <?=$pro_cat['cat_titulo']?></h2>
						<?php }elseif ($idioma == 'fra'){ ?>
							<h2>Ligne <?=$pro_cat['cat_titulo']?></h2>
						<?php } ?>
					</div>
					<a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" class="btn-hover">	
						<div class="project-content">
							<!--PORTUGUÊS-->
							<?php if ($idioma == 'pt'){ ?>
								<h2>Linha <?=$pro_cat['cat_titulo']?></h2>
								<a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title="Explorar produtos" class="seemore">Explorar produtos</a>
							<!--INGLÊS-->	
							<?php }elseif ($idioma == 'ing'){ ?>
								<h2><?=$pro_cat['cat_titulo']?> Line</h2>
								<a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title="Explore our products" class="seemore">Explore our Products</a>
							<!--ESPANHOL-->	
							<?php }elseif ($idioma == 'esp'){ ?>
								<h2>Línea <?=$pro_cat['cat_titulo']?></h2>
								<a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title="Explorar productos" class="seemore">Explorar productos</a>
							<!-- FRANCÊS -->
							<?php }elseif ($idioma == 'fra'){ ?>
								<h2>Ligne <?=$pro_cat['cat_titulo']?></h2>
								<a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title="Explorer les produits" class="seemore">Explorer les produits</a>
							<?php } ?>	
							
						</div>
					</a>
				</div>

			
			<?php 
				} 
			} 
			?>

		</div>

	</section>

	<!-- Testimonials section start -->
	<section class="testimonials-section spad">
		<div class="testimonials-image-box vendas"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-7 pl-lg-0 offset-lg-5 cta-content">
					<!--PORTUGUÊS-->
					<?php if ($idioma == 'pt'){ ?>
						<h1>Seja um<br><span>revendedor</span><br> Caemmun</h1>
						<p>
							Conheça a nossa marca e esteja sempre perto dos nossos clientes para apresentar nossos produtos.
						</p>
						<a href="revendedor" title="Encontrar" class="site-btn sb-light">Encontrar</a>
					<!--INGLÊS-->	
					<?php }elseif ($idioma == 'ing'){ ?>
						<h1>Become a <span>Caemmun</span><br> reseller</h1>  
						<p>
							Meet our brand and keep always close to our clients to present our products.
						</p>
						<a href="revendedor" title="Find" class="site-btn sb-light">Find</a>
					<!--ESPANHOL-->	
					<?php }elseif ($idioma == 'esp'){ ?>
						<h1>Sea un <span>distribuidor</span><br> Caemmun</h1>  
						<p>
							Conoce nuestra marca y quedaté siempre acerca de nuestros clientes para presentarles nuestros productos. 
						</p>
						<a href="revendedor" title="Encuentra" class="site-btn sb-light">Encuentra</a>
					<?php }elseif ($idioma == 'fra'){ ?>						  
						<h1>Devenez <span>Revendeur</span><br> Caemmun</h1>  
						<p>
							Découvrez notre marque et soyez toujours près de nos clients pour présenter nos produits.
						</p>
						<a href="revendedor" title="Trouver" class="site-btn sb-light">Trouver</a>
					<?php } ?>	
					
				</div>
			</div>
		</div>

	</section>

	<?php include 'includes/footer.php'; ?>
	<?php include 'includes/scripts.php'; ?>

	<script type="text/javascript">
		$(document).ready(function() {
		if ($('.hero-slider').hasClass('portuguese')) {
			$(".owl-prev").html("<i class='fa fa-long-arrow-left'></i>ANTERIOR");
			$(".owl-next").html("PRÓXIMO<i class='fa fa-long-arrow-right'></i>");
		}else if($('.hero-slider').hasClass('ingles')){
			$(".owl-prev").html("<i class='fa fa-long-arrow-left'></i>PREVIOUS");
			$(".owl-next").html("NEXT<i class='fa fa-long-arrow-right'></i>");
		}else if($('.hero-slider').hasClass('espanhol')){
			$(".owl-prev").html("<i class='fa fa-long-arrow-left'></i>ANTERIOR");
			$(".owl-next").html("SIGUIENTE<i class='fa fa-long-arrow-right'></i>");
		}
    });
	</script>

<script>
 $(function(){
		$(".close").on("click", function() {
      $(".overlay").addClass("fechar");
		});
  })
 
	
</script>

</body>
</html>