<?php
	include 'data/config.php';
	include 'data/setup.php';

	if ($idioma == 'pt'){ 
		$titulo = 'Contato';
	}elseif ($idioma == 'ing'){ 
		$titulo = 'Contact';
	}elseif ($idioma == 'esp'){
		$titulo = 'Contacto';
	}elseif ($idioma == 'fra'){
		$titulo = 'Contact';
	}  
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'includes/head.php'; ?>
</head>
<body>
	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>

	<!--PORTUGUÊS-->
	<?php if ($idioma == 'pt'){ ?>
		<section class="page-section pt100">
			<div class="container pb100">
				<div class="section-title pt-5">
					<h1>Entre em contato, fale com a Caemmun!</h1>
				</div>
				<div class="row">
					<div class="col-lg-3 contact-info mb-5 mb-lg-0">
						<h2>UNIDADE INDUSTRIAL DE ARAPONGAS, PR</h2>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i> Rua Juriti Vermelha, 279, Parque Industrial V, CEP 86702-280, Arapongas - PR, Brasil.</p>
						<p><i class="fa fa-phone"></i>+55 (43) 3172-6300</p>
						<p><i class="fa fa-at"></i> caemmun@caemmun.com.br</p>
					</div>
					<div class="col-lg-9">
						<form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
							<label>Seu Nome</label>
							<input type="text" name="nome" placeholder="Nome completo">
							<label>Seu Telefone</label>
							<input type="text" name="telefone" attrname="telephone1" placeholder="Ex: +99 (99) 99999-9999">
							<label>Seu E-mail</label>
							<input type="text" name="email" placeholder="Sua conta de e-mail">
							<label>Mensagem</label>
							<textarea name="mensagem" placeholder="Sua mensagem"></textarea>
							<div class="check"><input type="checkbox" id="check" name="check" value="concordo" checked>Li e concordo com a <a href="/site/termo/POLITICADEPRIVACIDADE.pdf">Política de privacidade</a></div>
							<input type="hidden" name="lang" value="pt">
							<input type="hidden" name="action_by_user" value="contato">
							<button type="button" id="enviar_mensagem" class="site-btn">Enviar</button>
						</form>
					</div>
				</div>
			</div>
		</section>
	<!--INGLÊS-->	
	<?php }elseif ($idioma == 'ing'){ ?>
		<section class="page-section pt100">
			<div class="container pb100">
				<div class="section-title pt-5">
					<h1>Get in contact, talk with Caemmun!</h1>
				</div>
				<div class="row">
					<div class="col-lg-3 contact-info mb-5 mb-lg-0">
						<h2>Industrial Unity in Arapongas, Paraná </h2>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i> Juriti Vermelha street, 279, Industrial Park V, <br>ZIP CODE 86702-280, Arapongas - PR, Brazil.</p>
						<p><i class="fa fa-phone"></i>+55 (43) 3172-6300</p>
						<p><i class="fa fa-at"></i> caemmun@caemmun.com.br</p>
					</div>
					<div class="col-lg-9">
						<form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
							<label>Your Name</label>
							<input type="text" name="nome" placeholder="Your Name">
							<label>Your Phone</label>
							<input type="text" name="telefone" attrname="telephone1" placeholder="Your Phone">
							<label>Your Email</label>
							<input type="text" name="email" placeholder="Your Email">
							<label>Message</label>
							<textarea name="mensagem" placeholder="Message"></textarea>
							<input type="hidden" name="lang" value="ing">
							<input type="hidden" name="action_by_user" value="contato">
							<button type="button" id="enviar_mensagem" class="site-btn">Send</button>
						</form>
					</div>
				</div>
			</div>
		</section>				
	<!--ESPANHOL-->	
	<?php }elseif ($idioma == 'esp'){ ?>
		<section class="page-section pt100">
			<div class="container pb100">
				<div class="section-title pt-5">
					<h1>¡Contacta con nosotros, habla con Caemmun!</h1>
				</div>
				<div class="row">
					<div class="col-lg-3 contact-info mb-5 mb-lg-0">
						<h2>UNIDAD INDUSTRIAL DE ARAPONGAS, PR</h2>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i> Calle Juriti Vermelha, 279, Parque Industrial V, <br>Código Postal 86702-280, Arapongas - PR, Brasil. </p>
						<p><i class="fa fa-phone"></i>+55 (43) 3172-6300</p>
						<p><i class="fa fa-at"></i> caemmun@caemmun.com.br</p>
					</div>
					<div class="col-lg-9">
						<form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
							<label>Su Nombre</label>
							<input type="text" name="nome" placeholder="Su Nombre">
							<label>Su Teléfono</label>
							<input type="text" name="telefone" placeholder="Su Teléfono">
							<label>Su Correo Electrónico</label>
							<input type="text" name="email" placeholder="Su Correo Electrónico">
							<label>Mensaje</label>
							<textarea name="mensagem" placeholder="Mensaje"></textarea>
							<input type="hidden" name="lang" value="esp">
							<input type="hidden" name="action_by_user" value="contato">
							<button type="button" id="enviar_mensagem" class="site-btn">Enviar</button>
						</form>
					</div>
				</div>
			</div>
		</section>						
	<?php }elseif ($idioma == 'fra'){ ?>
		<section class="page-section pt100">
			<div class="container pb100">
				<div class="section-title pt-5">
					<h1>Contactez-nous, parlez avec Caemmun!</h1>
				</div>
				<div class="row">
					<div class="col-lg-3 contact-info mb-5 mb-lg-0">
						<h2>UNITÉ INDUSTRIELLE ARAPONGAS, PR</h2>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> Rue Juriti Vermelha, 279, Parc industriel V, <br>Code postal 86702-280, Arapongas - PR, Brésil.</p>
                        <p><i class="fa fa-phone"></i>+55 (43) 3172-6300</p>
                        <p><i class="fa fa-at"></i> caemmun@caemmun.com.br</p>
					</div>
					<div class="col-lg-9">
						<form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
							<label>Votre Nom</label>
							<input type="text" name="nome" placeholder="Votre Nom">
							<label>Votre téléphone </label>
							<input type="text" name="telefone" placeholder="Votre téléphone ">
							<label>Votre E-mail</label>
							<input type="text" name="email" placeholder="Votre E-mail">
							<label>Message</label>
							<textarea name="mensagem" placeholder="Message"></textarea>
							<input type="hidden" name="lang" value="fra">
							<input type="hidden" name="action_by_user" value="contato">
							<button type="button" id="enviar_mensagem" class="site-btn">Envoyer</button>
						</form>
					</div>
				</div>
			</div>
		</section>						
	<?php } ?>



	<?php include 'includes/footer.php'; ?>
	<?php include 'includes/scripts.php'; ?>

	<?php if ($idioma == 'pt'){ ?>

		<script type="text/javascript">
	        function inputHandler(masks, max, event) {
            var c = event.target;
            var v = c.value.replace(/\D/g, '');
            var m = c.value.length > max ? 1 : 0;
            VMasker(c).unMask();
            VMasker(c).maskPattern(masks[m]);
            c.value = VMasker.toPattern(v, masks[m]);
            }

            var telMask = ['+99 (99) 9999-99999', '+99 (99) 99999-9999'];
            var tel = document.querySelector('input[attrname=telephone1]');
            VMasker(tel).maskPattern(telMask[0]);
            tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
		</script>

	<?php }elseif ($idioma == 'ing'){ ?>

		<script type="text/javascript">
	        function inputHandler(masks, max, event) {
            var c = event.target;
            var v = c.value.replace(/\D/g, '');
            var m = c.value.length > max ? 1 : 0;
            VMasker(c).unMask();
            VMasker(c).maskPattern(masks[m]);
            c.value = VMasker.toPattern(v, masks[m]);
            }

            var telMask = ['+9 (999) 999-9999'];
            var tel = document.querySelector('input[attrname=telephone1]');
            VMasker(tel).maskPattern(telMask[0]);
            tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
		</script>

	<?php }elseif ($idioma == 'esp'){ ?>

	<?php } ?>

	<script>
        var $form_contato = $('#contact-form');

        $(document).ready(function(){
            $form_contato.find('input, select, textarea').not('input[name=action_by_user], input[name=lang],  input[name=check]').val('');                
        })                                                        
        
        function validateForm($form)
        {
            var all_inputs = $form_contato.find('input, select, textarea, input[name=check]').not('input[name=action_by_user], input[name=lang]');                
            var $input;
            var valid = {valid: true, error: ''};

            all_inputs.each(function(index){
                $input = $(all_inputs[index]);

                if($input.val() == '')
                {   
                    console.log($input);
                    valid.valid = false;
                    valid.error = 'form';

                    $input.addClass('has-error');                        
                }
                else
                {
                    $input.removeClass('has-error');
                }
            })    
            
            return valid;
        }            
        
        $(document).on('click', '#enviar_mensagem', function(){                
            var form_valid = validateForm($form_contato);

            if(form_valid.valid)    
            {
            	var $button = $('#enviar_mensagem');

                var form_data = new FormData($form_contato[0]);

                $button.prop('disabled', true)
                
                $.ajax({
                    url: '<?= URL_BASE_ENVIO_AJAX ?>',
                    data: form_data,
                    type: 'POST',
                    processData: false,
                    contentType: false,

                    success: function(response){

                        response = JSON.parse(response);

                        $button.prop('disabled', true);

                        if(response.status == 'success')
                        {
                            swal({
                                title: 'Mensagem enviada com sucesso!',
                                text: 'Entraremos em contato em breve.',
                                type: 'success',
                                allowOutsideClick: false,
                            },
                            function (isConfirm) 
                            {
                                if(isConfirm)
                                {
                                    window.location.href = "home";
                                }
                            });                      
                        }
                        else
                        {
                            swal({
                                title: 'A mensagem não foi enviada!',
                                text: 'Tente novamente, se o problema persistir entre em contato através do nosso número: +55 (43) 3172-6300',
                                type: 'error'
                            });                      
                        }
                    },
                    error: function()
                    {
                        swal({
                            title: 'A mensagem não foi enviada!',
                            text: 'Tente novamente, se o problema persistir entre em contato através do nosso número: +55 (43) 3172-6300',
                            type: 'error'
                        }); 
                    }        
                })
            }
            else
            {
                if(form_valid.error == 'form')
                {
                    swal({
                        title: 'Ops!',
                        text: 'Certifique-se de completar todos os campos requeridos.',
                        type: 'error'
                    });    
                    
                }
            }
            
        })
        
    </script>
<!--     <script type="text/javascript">
    	$( document ).ready(function() {
		    $("#check").val("concordo");
		});
	</script> -->
	<script type="text/javascript">
		$(document).on('change', '#check', function() {
		    if(this.checked) {
		    	$('#enviar_mensagem').prop('disabled', false);
		    }else{
		    	$('#enviar_mensagem').prop('disabled', true);
		    }
		});
	</script>
</body>
</html>