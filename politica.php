<style>
	.politica_box{
		position: fixed;
		bottom: 0;
		width: 100%;
		margin: 0;
		padding: 0;
		left: 0;
		display: flex;
    	justify-content: center;
    	z-index: 99999999999;
	}
	.politica{
		position: relative;
		padding: 25px 10px;
	    background: #FFF;
	    opacity: .9;
	    width: 100%;
	    line-height: 50px;
	    align-items: center;
	    justify-content: center;
	    transition: opacity .5s linear;
	    width: 94%;
   		margin: 10px;
   		box-shadow: 0 2px 4px 0 rgba(0,0,0,0.4);
   		font-family: Calibri, Candara;
   		font-size: 13px;
   		border-radius: 2px;
		}
	.politica p{
		width: 100%;
		text-align: center;
		color: #424242;
		margin: 0;
	}
	.politica a{
		color: #0069de;
		text-decoration: underline;
	}
	.politica button{
		top: calc(50% - 20px);
		position: absolute;
	    background: #1c4d86;
	    border: none;
	    padding: 10px 18px;
	    color: white;
	    font-weight: 800;
	    right: 35px;
	    border-radius: 4px;
	    text-transform: uppercase;
	    cursor: pointer;
	    line-height: 20px;
	}
	.politica button:hover{
		text-decoration: underline;
	}
	.d-flex{
		content-visibility: visible;
		display: block;
	}
	.d-none{
		content-visibility: hidden;
		display: none;
	}
	.politica h4{
		font-weight: 600;
		font-size: 18px;
		line-height: 18px;
		color: #1c4d86;
		width: 100%;
		text-align: center;
		margin-bottom: 15px;
	}
	@media(max-width: 1800px){
		.politica button{
			right: calc(50% - 57px);
			bottom: 10px;
			position: sticky;
      		float: right;
      		line-height: 20px!important;
		}
		.politica{
			padding: 25px 22px 50px;
		}
		.politica p{
			text-align: justify;
			line-height: 28px;
			margin-bottom: 10px;
		}
		.politica h4{
      	text-align: center;
    	}
	}

    /** SAFARI ONLY */
      @media not all and (min-resolution: .001dpcm) {
        @supports(-webkit-appearance:none) and (stroke-color:transparent) {
        	.politica button{
	      		line-height: 20px!important;
			}
        }
      }
    /** SAFARI ONLY */
</style>
<div class="politica_box d-none">
	<div class="politica">
		<h4>Para uma melhor experiência em nosso site, por favor, aceite os nossos cookies de navegação.</h4>
		<p>Existem algumas opções que podem não funcionar sem a utilização dos cookies. Para mais informações sobre os cookies que utilizamos, visite a nossa <a href="/site/termo/POLITICADEPRIVACIDADE.pdf" target="_blank">Política de privacidade</a>.</p>
		<button type="button" id="concordo">Concordo</button>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="dev/js/js.cookie.min.js"></script>
<script>
	$(document).ready(function() {
		if (Cookies.get("politica_privacidade")) {
			$('.politica_box').addClass('d-none');
			$('.politica_box').removeClass('d-flex')
		}else{
			$('.politica_box').addClass('d-flex');
			$('.politica_box').removeClass('d-none');
		}
	
		$(document).on("click","#concordo",function() {

	        Cookies.set("politica_privacidade", "Concordo", { expires: 30 });
	        $('.politica_box').removeClass('d-flex');
	        $('.politica_box').addClass('d-none');
	    });
    });
</script>