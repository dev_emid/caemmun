<!-- Page Preloder -->
<div id="preloder">
	<div class="loader"></div>
</div>

<!-- Header section start -->   
<header class="header-area">
	<a href="home" title="Caemmun Movelaria" class="logo-area">
		<img src="dev/img/caemmun.svg" alt="">
	</a>
	<div class="nav-switch">
		<i class="fa fa-bars"></i>
	</div>
	<div class="phone-number">
		<div class="idiomas">
				<div class="pt <?=$idioma == 'pt' ? 'idioma_selecionado' : ''?>">
					<figure>
						<img src="dev/img/icon/brasil.svg" alt="Idioma Português">
					</figure>
				</div>
				<div class="ing <?=$idioma == 'ing' ? 'idioma_selecionado' : ''?>">
					<figure>
						<img src="dev/img/icon/estados-unidos.svg" alt="Idioma Inglês">
					</figure>
				</div>
				<div class="esp <?=$idioma == 'esp' ? 'idioma_selecionado' : ''?>">
					<figure>
						<img src="dev/img/icon/espanha.svg" alt="Idioma Espanhol">
					</figure>
				</div>
				<div class="fra <?=$idioma == 'fra' ? 'idioma_selecionado' : ''?>">
					<figure>
						<img src="dev/img/icon/france.png" alt="Idioma Francês">
					</figure>
				</div>
		</div>
	</div>
	<?php if ($idioma == 'pt'){ ?>
		<nav class="nav-menu">
			<ul>
				<li><a href="sobre">SOBRE NÓS</a></li>
				<li>
					<a href="#">PRODUTOS</a>
					<ul class="dropdown">
						<?php 

						$cat_0 = mysqli_query($connect, "SELECT DISTINCT cat_mae FROM tb_categorias INNER JOIN tab_produtos ON(tab_produtos.pro_categoria = tb_categorias.cat_id) WHERE pro_status = 'A'");
	
						while ($cat_head = mysqli_fetch_assoc($cat_0)) { 
							$mae = $cat_head['cat_mae'];

						$categor = mysqli_query($connect, "SELECT cat_titulo FROM tb_categorias WHERE cat_id = {$mae} ORDER BY cat_id ASC");

							while($pro_cat = mysqli_fetch_assoc($categor)){

							?>

						<li><a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title=""><?=$pro_cat['cat_titulo']?></a></li>

						<?php 
							} 
						} 
						?>
					</ul>
				</li>
				<li><a href="revendedor">SEJA REVENDEDOR</a></li>
				<li>
					<a href="javascript:void(0)">REPRESENTANTES</a>
					<ul class="dropdown">
						<li><a href="http://extranet.caemmun.com.br/login/auth" rel="noreferrer" target="_blank">Extranet</a></li>
						<li><a href="https://webmail-seguro.com.br/caemmun.com.br/" rel="noreferrer" target="_blank">Webmail</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)">ÁREA DO CLIENTE</a>
					<ul class="dropdown">
						<li><a href="contato">Fale Conosco</a></li>
						<li><a href="https://caemmun.portaldocliente.online/" rel="noreferrer" target="_blank">Portal do Cliente</a></li>
					</ul>
				</li>
				<!-- <li><a href="contato">FALE CONOSCO</a></li> -->
				<li><a href="suporte">SUPORTE AO PRODUTO</a></li>
				<div class="idiomas mm">
					<div class="pt <?=$idioma == 'pt' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/brasil.svg" alt="Idioma Português">
						</figure>
					</div>
					<div class="ing <?=$idioma == 'ing' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/estados-unidos.svg" alt="Idioma Inglês">
						</figure>
					</div>
					<div class="esp <?=$idioma == 'esp' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/espanha.svg" alt="Idioma Espanhol">
						</figure>
					</div>
					<div class="fra <?=$idioma == 'fra' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/france.png" alt="Idioma Francês">
						</figure>
					</div>
				</div>
			</ul>
		</nav>
	<?php } ?>	
	<?php if ($idioma == 'ing'){ ?>
		<nav class="nav-menu">
			<ul>
				<li><a href="sobre">ABOUT US</a></li>
				<li>
					<a href="#">PRODUCTS</a>
					<ul class="dropdown">
						<?php 

						$cat_0 = mysqli_query($connect, "SELECT DISTINCT cat_mae FROM tb_categorias INNER JOIN tab_produtos ON(tab_produtos.pro_categoria = tb_categorias.cat_id) WHERE pro_status = 'A'");
	
						while ($cat_head = mysqli_fetch_assoc($cat_0)) { 
							$mae = $cat_head['cat_mae'];

						$categor = mysqli_query($connect, "SELECT cat_titulo FROM tb_categorias WHERE cat_id = {$mae} ORDER BY cat_id ASC");

							while($pro_cat = mysqli_fetch_assoc($categor)){

							?>

						<li><a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title=""><?=$pro_cat['cat_titulo']?></a></li>

						<?php 
							} 
						} 
						?>
					</ul>
				</li>
				<li><a href="revendedor">BECOME A RESELLER</a></li>
				<li>
					<a href="javascript:void(0)" rel="noreferrer">REPRESENTATIVES</a>
					<ul class="dropdown">
						<li><a href="http://extranet.caemmun.com.br/login/auth" rel="noreferrer" target="_blank">Extranet</a></li>
						<li><a href="https://webmail-seguro.com.br/caemmun.com.br/" rel="noreferrer" target="_blank">Webmail</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)">CUSTOMER'S AREA</a>
					<ul class="dropdown">
						<li><a href="contato">Contact</a></li>
						<li><a href="https://caemmun.portaldocliente.online/" rel="noreferrer" target="_blank">Customer Portal</a></li>
					</ul>
				</li>
				<!-- <li><a href="contato">CONTACT US</a></li> -->
				<li><a href="suporte">PRODUCT SUPPORT</a></li>
				<div class="idiomas mm">
					<div class="pt <?=$idioma == 'pt' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/brasil.svg" alt="Idioma Português">
						</figure>
					</div>
					<div class="ing <?=$idioma == 'ing' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/estados-unidos.svg" alt="Idioma Inglês">
						</figure>
					</div>
					<div class="esp <?=$idioma == 'esp' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/espanha.svg" alt="Idioma Espanhol">
						</figure>
					</div>
					<div class="fra <?=$idioma == 'fra' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/france.png" alt="Idioma Francês">
						</figure>
					</div>
				</div>
			</ul>
		</nav>
	<?php } ?>
	<?php if ($idioma == 'esp'){ ?>
		<nav class="nav-menu">
			<ul>
				<li><a href="sobre">SOBRE NOSOTROS</a></li>
				<li>
					<a href="#">PRODUCTOS</a>
					<ul class="dropdown">
						<?php 

						$cat_0 = mysqli_query($connect, "SELECT DISTINCT cat_mae FROM tb_categorias INNER JOIN tab_produtos ON(tab_produtos.pro_categoria = tb_categorias.cat_id) WHERE pro_status = 'A'");
	
						while ($cat_head = mysqli_fetch_assoc($cat_0)) { 
							$mae = $cat_head['cat_mae'];

						$categor = mysqli_query($connect, "SELECT cat_titulo FROM tb_categorias WHERE cat_id = {$mae} ORDER BY cat_id ASC");

							while($pro_cat = mysqli_fetch_assoc($categor)){

							?>

						<li><a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title=""><?=$pro_cat['cat_titulo']?></a></li>

						<?php 
							} 
						} 
						?>
					</ul>
				</li>
				<li><a href="revendedor">SEA DISTRIBUIDOR</a></li>
				<li>
					<a href="javascript:void(0)" rel="noreferrer">DISTRIBUIDORES</a>
					<ul class="dropdown">
						<li><a href="http://extranet.caemmun.com.br/login/auth" rel="noreferrer" target="_blank">Extranet</a></li>
						<li><a href="https://webmail-seguro.com.br/caemmun.com.br/" rel="noreferrer" target="_blank">Webmail</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)">ÁREA DE CLIENTES</a>
					<ul class="dropdown">
						<li><a href="contato">Contacto</a></li>
						<li><a href="https://caemmun.portaldocliente.online/" rel="noreferrer" target="_blank">Portal del Cliente</a></li>
					</ul>
				</li>
				<!-- <li><a href="contato">CONTACTO</a></li> -->
				<li><a href="suporte">SOPORTE DE PRODUCTO</a></li>
				<div class="idiomas mm">
					<div class="pt <?=$idioma == 'pt' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/brasil.svg" alt="Idioma Português">
						</figure>
					</div>
					<div class="ing <?=$idioma == 'ing' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/estados-unidos.svg" alt="Idioma Inglês">
						</figure>
					</div>
					<div class="esp <?=$idioma == 'esp' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/espanha.svg" alt="Idioma Espanhol">
						</figure>
					</div>
					<div class="fra <?=$idioma == 'fra' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/france.png" alt="Idioma Francês">
						</figure>
					</div>
				</div>
			</ul>
		</nav>
	<?php } ?>
	<?php if ($idioma == 'fra'){ ?>
		<nav class="nav-menu">
			<ul>
				<li><a href="sobre">QUI SOMMES NOUS</a></li>
				<li>
					<a href="#">PRODUITS</a>
					<ul class="dropdown">
						<?php 

						$cat_0 = mysqli_query($connect, "SELECT DISTINCT cat_mae FROM tb_categorias INNER JOIN tab_produtos ON(tab_produtos.pro_categoria = tb_categorias.cat_id) WHERE pro_status = 'A'");
	
						while ($cat_head = mysqli_fetch_assoc($cat_0)) { 
							$mae = $cat_head['cat_mae'];

						$categor = mysqli_query($connect, "SELECT cat_titulo FROM tb_categorias WHERE cat_id = {$mae} ORDER BY cat_id ASC");

							while($pro_cat = mysqli_fetch_assoc($categor)){

							?>

						<li><a href="produtos?cat=<?=urlencode($pro_cat['cat_titulo'])?>&sub=Todos" title=""><?=$pro_cat['cat_titulo']?></a></li>

						<?php 
							} 
						} 
						?>
					</ul>
				</li>
				<li><a href="revendedor">DEVENEZ REVENDEUR</a></li>
				<li>
					<a href="javascript:void(0)">REPRÉSENTENTS</a>
					<ul class="dropdown">
						<li><a href="http://extranet.caemmun.com.br/login/auth" rel="noreferrer" target="_blank">Extranet</a></li>
						<li><a href="https://webmail-seguro.com.br/caemmun.com.br/" rel="noreferrer" target="_blank">Webmail</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)">ESPACE CLIENT</a>
					<ul class="dropdown">
						<li><a href="contato">Prendre contact</a></li>
						<li><a href="https://caemmun.portaldocliente.online/" rel="noreferrer" target="_blank">Portail Clients</a></li>
					</ul>
				</li>
				<!-- <li><a href="contato">CONTACTEZ-NOUS</a></li> -->
				<li><a href="suporte">ASSISTANCE PRODUIT</a></li>
				<div class="idiomas mm">
					<div class="pt <?=$idioma == 'pt' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/brasil.svg" alt="Idioma Português">
						</figure>
					</div>
					<div class="ing <?=$idioma == 'ing' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/estados-unidos.svg" alt="Idioma Inglês">
						</figure>
					</div>
					<div class="esp <?=$idioma == 'esp' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/espanha.svg" alt="Idioma Espanhol">
						</figure>
					</div>
					<div class="fra <?=$idioma == 'fra' ? 'idioma_selecionado' : ''?>">
						<figure>
							<img src="dev/img/icon/france.png" alt="Idioma Francês">
						</figure>
					</div>
				</div>
			</ul>
		</nav>
	<?php } ?>	
</header>
<!-- Header section end --> 

