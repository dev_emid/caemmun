<!-- Footer section start -->
<footer class="footer-section">
	<div class="footer-social">
		<div class="social-links">
			<a href="https://br.pinterest.com/caemmunmovelaria/_created/" title="Pinterest" rel="noreferrer" target="_blank"><i class="fa fa-pinterest"></i></a>
			<a href="https://www.youtube.com/channel/UCfJiKOIwwx-v6DeVE5mxLSQ" title="Youtube" rel="noreferrer" target="_blank"><i class="fa fa-youtube"></i></a>
			<a href="https://www.instagram.com/caemmunmovelaria/" title="Instagram" rel="noreferrer" target="_blank"><i class="fa fa-instagram"></i></a>
			<a href="https://www.facebook.com/caemmun" title="Facebook" rel="noreferrer" target="_blank"><i class="fa fa-facebook"></i></a>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-9 offset-lg-3">
				<div class="row">
					<div class="col-md-4">
						<div class="footer-item">
							<ul>
								<li><a href="produtos?cat=Caemmun&sub=Todos">Caemmun</a></li>
								<li><a href="produtos?cat=Caemmun+Star&sub=Todos">Caemmun Star</a></li>
								<li><a href="produtos?cat=Caemmun+Office&sub=Todos">Caemmun Office</a></li>
								<!-- <li><a href="produtos?cat=Caemmun+Masterchef&sub=Todos">Caemmun Masterchef</a></li> -->
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="footer-item">
							<ul>
								<!--PORTUGUÊS-->
								<?php if ($idioma == 'pt'){ ?>
									<li><a href="home">Inicial</a></li>
									<li><a href="sobre">Sobre Nós</a></li>
									<li><a href="revendedor">Seja Revendedor</a></li>
									<li><a href="http://extranet.caemmun.com.br/login/auth" target="_blank">Representantes</a></li>
									<li><a href="trabalhe">Trabalhe Conosco</a></li>
									<li><a href="contato">Contato</a></li>
								<!--INGLÊS-->	
								<?php }elseif ($idioma == 'ing'){ ?>
									<li><a href="home">Home</a></li>
									<li><a href="sobre">About Us</a></li>
									<li><a href="revendedor">Become a Reseller</a></li>
									<li><a href="http://extranet.caemmun.com.br/login/auth" target="_blank">Representatives</a></li>

									<li><a href="trabalhe">Work with us</a></li>
									<li><a href="contato">Contact</a></li>
								<!--ESPANHOL-->	
								<?php }elseif ($idioma == 'esp'){ ?>
									<li><a href="home">Inicio</a></li>
									<li><a href="sobre">Sobre Nosotros</a></li>
									<li><a href="revendedor">Sea Distribuidor</a></li>
									<li><a href="http://extranet.caemmun.com.br/login/auth" target="_blank">Distribuidores</a></li>
									<li><a href="trabalhe">Trabaja con nosotros</a></li>
									<li><a href="contato">Contacto</a></li>
								<?php }elseif ($idioma == 'fra'){ ?>
									<li><a href="home">Page d'accueil</a></li>
									<li><a href="sobre">Qui Sommes Nous</a></li>
									<li><a href="revendedor">Devenez Revendeur </a></li>
									<li><a href="http://extranet.caemmun.com.br/login/auth" target="_blank">Représentents </a></li>
									<li><a href="trabalhe">Travaillez avec nous</a></li>
									<li><a href="contato">Contactez-Nous</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="footer-item">
							<img src="dev/img/gptw.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="copyright">
	<!--PORTUGUÊS-->
	<?php if ($idioma == 'pt'){ ?>
		<a href="home" title="Copyright"><img class="logo_footer" src="dev/img/caemmun_footer.svg" alt=""></a>
		As imagens dos produtos são meramente ilustrativas,<br>podendo ser alteradas sem prévio aviso.<br>
		Copyright &copy; <script>document.write(new Date().getFullYear());</script> - Todos os direitos reservados. <br>Desenvolvido por <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="https://e-mid.com.br" rel="noreferrer" title="E-MID" target="_blank">E-MID</a>
	<!--INGLÊS-->	
	<?php }elseif ($idioma == 'ing'){ ?>
		<a href="home" title="Copyright"><img class="logo_footer" src="dev/img/caemmun_footer.svg" alt=""></a>
		Product images are for illustration purposes only,<br>
		may be changed without prior notice.<br>
		Copyright &copy; <script>document.write(new Date().getFullYear());</script> - All rights reserved. <br> Developed by <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="https://e-mid.com.br" rel="noreferrer" title="E-MID" target="_blank">E-MID</a>
	<!--ESPANHOL-->	
	<?php }elseif ($idioma == 'esp'){ ?>
		<a href="home" title="Copyright"><img class="logo_footer" src="dev/img/caemmun_footer.svg" alt=""></a>
		Las imágenes del producto son solo para fines ilustrativos,<br>	
		puede cambiar sin previo aviso.<br>
		Copyright &copy; <script>document.write(new Date().getFullYear());</script> - Todos los derechos reservados. <br> Desarrollado por <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="https://e-mid.com.br" rel="noreferrer" title="E-MID" target="_blank">E-MID</a>
	<?php }elseif ($idioma == 'fra'){ ?>
		<a href="home" title="Copyright"><img class="logo_footer" src="dev/img/caemmun_footer.svg" alt=""></a>
		Les images des produits sont à titre indicatif seulement,<br> peuvent être modifiées sans préavis.<br>
		Copyright &copy; <script>document.write(new Date().getFullYear());</script> - Tous les droits sont réservés. <br> Développé par <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="https://e-mid.com.br" rel="noreferrer" title="E-MID" target="_blank">E-MID</a>
	<?php } ?>		
</div>
</footer>
<!-- Footer section end -->