<?php include 'politica.php'; ?>
<!--====== Javascripts & Jquery ======-->
<script src="dev/js/jquery-2.1.4.min.js"></script>
<script src="dev/js/bootstrap.min.js"></script>
<script src="dev/js/isotope.pkgd.min.js"></script>
<script src="dev/js/owl.carousel.min.js"></script>
<script src="dev/js/jquery.owl-filter.js"></script>
<script src="dev/js/magnific-popup.min.js"></script>
<script src="dev/js/circle-progress.min.js"></script>
<script src="dev/js/main.js"></script>
<script src="dev/js/js.cookie.min.js"></script>
<script src="dev/js/sweetalert.min.js"></script>
<script src="dev/js/mask-vanilla.js"></script>
<script type="text/javascript">
	$(document).on("click",".pt",function() {
		Cookies.set("idioma", "pt", { expires: 30 });
		location.reload();
	});
	$(document).on("click",".esp",function() {
		Cookies.set("idioma", "esp", { expires: 30 });
		location.reload();
	});
	$(document).on("click",".ing",function() {
		Cookies.set("idioma", "ing", { expires: 30 });
		location.reload();
	});
	$(document).on("click",".fra",function() {
		Cookies.set("idioma", "fra", { expires: 30 });
		location.reload();
	});
</script> 

<?php 
	if (isset($_GET['idioma']) && $_GET['idioma'] == 'frances') { ?>
	<script type="text/javascript">
		Cookies.set("idioma", "fra", { expires: 30 });
	</script>
<?php 
    }
?> 
<!-- <script src="https://kit.fontawesome.com/a076d05399.js"></script> -->