<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=G-J0L8KK8NHH"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-J0L8KK8NHH');
</script> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-64279861-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-64279861-1');
</script>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Caemmun Movelaria</title>


<meta name="distribution" content="Global">
<meta name="revisit-after" content="7 days">
<meta name="googlebot" content="index, follow">
<meta name="robots" content="all, index, follow">
<meta name="description" content="Em mais de 20 anos de atuação, nossa história foi marcada por muito trabalho e dedicação de todos os colaboradores, fornecedores e parceiros.">
<meta name="keywords" content="Caemmun, caemmun, estante,estantes, estante rack, rack com painel, rack para tv, rack para sala, rack para sala de tv, rack suspenso, racks, prateleira, estantes para sala, estante para sala pequena, estante para sala, estante para sala de tv, painel tv, painel para tv, painel para tv com rack, painel para tv quarto, painéis para tv, home suspenso, home suspenso para tv, home theater, home theater suspenso, sala de estar, sala de tv, sala de jantar, escritório, móveis para sala, móveis para sala de jantar, móveis para escritório, moveis para sala de jantar, moveis para sala">
<meta name="author" content="E-MID Agência Digital">

<!-- Stylesheets -->
<link rel="stylesheet" href="dev/css/bootstrap.min.css"/>
<!-- <link rel="stylesheet" href="dev/css/font-awesome.min.css"/> -->
<link rel="stylesheet" href="dev/css/animate.css"/>
<link rel="stylesheet" href="dev/css/owl.carousel.css"/>
<link rel="stylesheet" href="dev/css/sweetalert.min.css">
<link rel="stylesheet" href="dev/css/style.css"/>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

<?php $url = $_SERVER["REQUEST_URI"]; ?>

<meta property="og:locale" content="pt_BR">
<meta property="og:site_name" content="Caemmun Movelaria">
<meta property="og:url" content="https://caemmun.com.br/<?=$url?>">
<meta property="og:type" content="website">
<meta property="og:og:email" content="contato@caemmun.com.br">
<meta property="og:phone_number" content="(43) 3172-6300">

<!------------METAS BLOG------------->
<?php if ($pagina == 'produto'){ ?>
<?php $metas = mysqli_query($connect, "SELECT * FROM tab_produtos INNER JOIN tb_fotos ON (tb_fotos.fot_vinculo = tab_produtos.pro_id) WHERE fot_modulo = 'produtos' AND fot_capa = 1 AND pro_id = '$id' AND pro_status = 'A'");
   while($meta = mysqli_fetch_array($metas)){
   ?>
<meta property="og:title" content="<?=$meta['pro_titulo']?>">
<meta property="og:image" content="<?=PATH_PRODUTOS?><?=$id?>/original/<?=$meta['fot_titulo']?>" alt="<?=$meta['pro_titulo']?>">
<?php $desc = strip_tags(mb_strimwidth($meta['pro_descricao'], "0", "300", "...")); ?>
<meta property="og:description" content="<?php echo $desc; ?>">
<?php }
   }
   else{ 
   ?>
<meta property="og:title" content="Caemmun Movelaria">
<meta property="og:image" content="dev/img/og.png">
<meta property="og:description" content="Em mais de 20 anos de atuação, nossa história foi marcada por muito trabalho e dedicação de todos os colaboradores, fornecedores e parceiros.">
<?php } ?>
<meta property="og:street_address" content="Rua Juriti Vermelha, 279 - Parque Industrial V">
<meta property="og:postal-code" content="86702-280">

<link rel="apple-touch-icon" sizes="57x57" href="dev/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="dev/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="dev/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="dev/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="dev/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="dev/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="dev/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="dev/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="dev/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="dev/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="dev/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="dev/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="dev/img/favicon/favicon-16x16.png">
<link rel="manifest" href="dev/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="dev/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">