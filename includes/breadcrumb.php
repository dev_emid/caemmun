<!-- Page header section start -->
<section class="page-header-section set-bg" data-setbg="dev/img/breadcrumb.webp">
	<div class="container">
		<h1 class="header-title"><?= $titulo; ?><span>.</span></h1>
	</div>
</section>
<!-- Page header section end -->