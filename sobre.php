<?php
	include 'data/config.php';
	include 'data/setup.php';

	if ($idioma == 'pt'){ 
		$titulo = 'Sobre Nós';
	}elseif ($idioma == 'ing'){ 
		$titulo = 'About Us';
	}elseif ($idioma == 'esp'){
		$titulo = 'Sobre Nosotros';
	}elseif ($idioma == 'fra'){
		$titulo = 'À Propos De Nous';
	} 
?>
	 
<!DOCTYPE html>
<html>
<head>
	<?php include 'includes/head.php'; ?>
</head>
<body>
	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>
		<!--PORTUGUÊS-->
		<?php if ($idioma == 'pt'){ ?>
			<section class="intro-section spad">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 intro-text">
							<h1>CONHEÇA MAIS SOBRE A <span>CAEMMUN</span></h1>
							<div class="row">
								<div class="col-md-6">
									<p>
										A CAEMMUN MOVELARIA é uma empresa brasileira e está sediada em Arapongas (PR), um dos maiores polos moveleiros do país.
										Com um know-how de 30 anos no desenvolvimento de soluções em mobiliários, a Caemmun faz parte do Grupo Munhoz Caetano que integra outras grandes marcas. Com duas plantas fabris sendo uma em Arapongas e outra em Sabáudia que juntas possuem mais de 150.000m² de área construída.
										A Caemmun atua em todos os estados do país, atendendo desde os pequenos lojistas até os grandes magazines, com mais de 5 mil clientes ativos, também possui uma notável presença no mercado 										
									</p>
								</div>
								<div class="col-md-6">
									<p>	internacional, exportando seu mix para mais de 60 países, com centro de distribuição na Florida/EUA e com a abertura de uma nova marca Líder Design nos Estados Unidos.
										Sempre preocupada com a preservação de recursos naturais que compõe sua cadeia produtiva, foi criada a Caemmun Reflorestamento, que consiste em fazendas de plantio de Eucalipto e Pinus.
										Atenta as oportunidades de mercado, bem como investindo constantemente em estrutura fabril, tecnologia e, principalmente em pessoas, o Grupo Munhoz Caetano tem como objetivo firmar-se como um grande gerador de soluções no segmento moveleiro e outros relacionados ao seu setor.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 pt-4 sobre_img">
							<img src="dev/img/instituciona-interna.webp" alt="Sobre Nós">
						</div>
					</div>
				</div>
			</section>

			<section class="testimonials-section spad">
				<div class="testimonials-image-box"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-7 pl-lg-0 offset-lg-5 cta-content">
							<h1>Nossos<span> Princípios</span></h1>
							<div class="testimonials-slider" id="test-slider">
								<div class="ts-item">
									<h2>Missão</h2>
									<p>Fornecer as melhores soluções para o mercado moveleiro.</p>
								</div>
								<div class="ts-item">
									<h2>Visão</h2>
									<p>Estar entre os principais provedores de móveis do Brasil</p>
								</div>
								<div class="ts-item">
									<h2>Valores</h2>
									<p>
										Busca da Evolução; Olhar do dono; Visão sistêmica; Melhoria contínua; Simplicidade; Respeito.
									</p>
								</div>
								<div class="ts-item">
									<h2>Propósito</h2>
									<p>
										Transformar ambientes, facilitando o viver.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		<!--INGLÊS-->	
		<?php }elseif ($idioma == 'ing'){ ?>
			<section class="intro-section spad">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 intro-text">
							<h1>MEET <span>CAEMMUN</span></h1>
							<div class="row">
								<div class="col-md-6">
									<p>
										Know more about Caemmun. Caemmun furniture is a Brazilian factory based in Arapongas, Paraná, one of the biggest furniture pole in the whole country. With a 30 years know-how in the furniture solutions development, Caemmun is part of Group Munhoz Caetano, which integrate other big brands. We have two manufacturing plants being one in Arapongas and another one in Sabáudia, which together have more than 150.000m² of constructed area. Caemmun act in all the country's states, attending since the small stores up to the huge retailers, more than 5 thousand active clients, also have a remarkable presence in the international market, 
										
									</p>
								</div>
								<div class="col-md-6">
									<p>
										exporting its mix for over 60 countries, with a distribution center in Florida/USA and with the opening of a new brand, Líder Design in United States. Always committed with the preservation of the natural sources that compound it manufacturing chain, We create Caemmun Reforestation, which consists in planting forests of Eucalyptus and Pinus. Attentive to the market opportunities, as well as constantly investing in productive structure, technology and principally in people, Munhoz Caetano Group aims to establish itself as a major generator of solutions in the furniture segment and others related to its sector.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 pt-4 sobre_img">
							<img src="dev/img/revendedor.webp" alt="Sobre Nós">
						</div>
					</div>
				</div>
			</section>

			<section class="testimonials-section spad">
				<div class="testimonials-image-box"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-7 pl-lg-0 offset-lg-5 cta-content">
							<h1>Our <span> Principles</span></h1>
							<div class="testimonials-slider" id="test-slider">
								<div class="ts-item">
									<h2>Mission</h2>
									<p>Provide the best solutions to the furniture market.</p>
								</div>
								<div class="ts-item">
									<h2>Vision</h2>
									<p>Stay among the principal furniture providers in Brazil.</p>
								</div>
								<div class="ts-item">
									<h2>Values</h2>
									<p>
										Search for Evolution; Owner look; Systemic Vision; Constant improvement; Simplicity; Respect.
									</p>
								</div>
								<div class="ts-item">
									<h2>Purpose</h2>
									<p>
										Transform environments, making life easier
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>				
		<!--ESPANHOL-->	
		<?php }elseif ($idioma == 'esp'){ ?>
			<section class="intro-section spad">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 intro-text">
							<h1>CONOZCA MÁS SOBRE <span>CAEMMUN</span></h1>  
							<div class="row">
								<div class="col-md-6">
									<p>
										Conozca más sobre Caemmun CAEMMUN MOVELARIA es una empresa brasileña y tiene su sede en Arapongas (PR), uno de los centros de muebles más grandes del país. Con 30 años de experiencia en el desarrollo de soluciones de mobiliario, Caemmun forma parte del Grupo Munhoz Caetano, que integra otras grandes marcas. Con dos plantas de fabricación, una en Arapongas y otra en Sabáudia, que en conjunto suman más de 150.000 m² de superficie construida. Caemmun opera en todos los estados del país, atendiendo desde pequeños minoristas hasta grandes tiendas, con más de 5,000 clientes activos, además tiene una presencia notable en el mercado internacional, 
										
									</p>
								</div>
								<div class="col-md-6">
									<p>	exportando su mix a más de 60 países, con un centro de distribución en Florida. / EUA y la apertura de una nueva marca Lider Design en Estados Unidos. 	Siempre preocupada por la preservación de los recursos naturales que componen su cadena productiva, se creó Caemmun Reflorestamento, que consiste en plantaciones de Eucalipto y Pinus. Atento a las oportunidades del mercado, además de invertir constantemente en estructura de fabricación, tecnología y, principalmente, en las personas, el Grupo Munhoz Caetano pretende consolidarse como un gran generador de soluciones en el segmento del mueble y otros relacionados con su sector.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 pt-4 sobre_img">
							<img src="dev/img/sob.jpg" alt="Sobre Nós">
						</div>
					</div>
				</div>
			</section>

			<section class="testimonials-section spad">
				<div class="testimonials-image-box"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-7 pl-lg-0 offset-lg-5 cta-content">
							<h1>Nuestros <span> Principios</span></h1>
							<div class="testimonials-slider" id="test-slider">
								<div class="ts-item">
									<h2>Misión</h2>
									<p>Brindar las mejores soluciones para el mercado de muebles.</p>
								</div>
								<div class="ts-item">
									<h2>Visión</h2>
									<p>Estar entre los principales proveedores de muebles de Brasil.</p>
								</div>
								<div class="ts-item">
									<h2>Valores</h2>
									<p>
										Búsqueda por la evolución; vista del propietario; Visión sistémica; Mejora continua; Sencillez; El respeto.
									</p>
								</div>
								<div class="ts-item">
									<h2>Propósito</h2>
									<p>
										Transforma los entornos, haciéndote la vida más fácil.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>						
		<?php }elseif ($idioma == 'fra'){ ?>
			<section class="intro-section spad">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 intro-text">
							<h1>APPRENEZ EN-PLUS DE <span>CAEMMUN</span></h1>  
							<div class="row">
								<div class="col-md-6">
									<p>
										Apprenez en-plus sur Caemmun. CAEMMUN  MOVELARIA est une enterprise brésilienne basée à Arapongas (PR), l´un des plus grands pôles du meuble du pays. Avec un know-how de 30 années d'expérience dans le développement de solutions dans le domaine du meuble, Caemmun fait partie du Groupe Munhoz Caetano, qui intègre plusieurs d´autres marques. Elle dispose de deux usines, l´une située à Arapongas et l´autre à Sabaudia, lesquelles posèdent ensemble 150.000m² de surface construite. Caemmun opère dans tous les états du pays, en répondant aux besoins aussi bien des petits commerçants que de grandes magasins, avec 5.000 clients actifs. Elle a aussi développé une présence internationale considérable,
										
									</p>
								</div>
								<div class="col-md-6">
									<p>	en exportant sa gamme de produits vers plus de 60 pays, avec un centre de distribution à Florida/EUA et la creation d´une nouvelle marque Líder Design aux États Unis. Toujours préoccupé par la préservation des resources naturelles qui font partie de sa chaîne de production, Caemmun Reflorestamento fut crée. Elle est composée de fôrets d´eucalyptus et pins. Attentive aux opportunités du marché, avec des investissements réguliers dans la structure productive, technologie, et principalment dans les gens, Le Groupe Munhoz Caetano vise s´affirmer comme un grand générateur des solutions dans le secteur du meuble et des autres segments associés.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 pt-4 sobre_img">
							<img src="dev/img/sob.jpg" alt="Sobre Nós">
						</div>
					</div>
				</div>
			</section>

			<section class="testimonials-section spad">
				<div class="testimonials-image-box"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-7 pl-lg-0 offset-lg-5 cta-content">
							<h1>Nos <span> principes</span></h1>
							<div class="testimonials-slider" id="test-slider">
								<div class="ts-item">
									<h2>Mission</h2>
									<p>Fournir les meilleures solutions pour le marché du meuble</p>
								</div>
								<div class="ts-item">
									<h2>Vision</h2>
									<p>Être parmi les principaux fournisseurs des meubles du Brésil</p>
								</div>
								<div class="ts-item">
									<h2>Valeurs</h2>
									<p>
										Évolution; Regard du Maître; Vision systémique; Amélioration constante; Simplicité; Respect
									</p>
								</div>
								<div class="ts-item">
									<h2>Objectif</h2>
									<p>
										Transformer les ambiances; faciliter la vie.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>						
		<?php } ?>
	

	<?php include 'includes/footer.php'; ?>
	<?php include 'includes/scripts.php'; ?>
</body>
</html>