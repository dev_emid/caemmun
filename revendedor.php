<?php
	include 'data/config.php';
	include 'data/setup.php';

	if ($idioma == 'pt'){ 
		$titulo = 'Seja Revendedor';
	}elseif ($idioma == 'ing'){ 
		$titulo = 'Become a Reseller';
	}elseif ($idioma == 'esp'){
		$titulo = 'Sea Distribuidor';
	} elseif ($idioma == 'fra'){
        $titulo = 'Devenez Revendeur';
    } 
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'includes/head.php'; ?>
</head>
<body>
	<?php include 'includes/header.php'; ?>

	<!-- Breadcrumb -->
	<?php include 'includes/breadcrumb.php'; ?>

	<!--PORTUGUÊS-->
	<?php if ($idioma == 'pt'){ ?>
		<section class="page-section pt100">
			<div class="container pb100">
				<div class="section-title pt-5">
					<h1>Envie uma mensagem, seja um revendedor Caemmun!</h1>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
							<div class="row">
								<div class="col-lg-6 col-md-12">
									<label>Razão Social</label>
									<input type="text" name="razao_social" placeholder="Razão Social">
								</div>
								<div class="col-lg-6 col-md-12">
									<label>Nome Fantasia</label>
									<input type="text" name="nome_fantasia" placeholder="Nome Fantasia">
								</div>	
								<div class="col-lg-6 col-md-12">
									<label>CNPJ</label>
									<input type="text" name="cnpj" attrname="cnpj" placeholder="CNPJ">
								</div>
								<div class="col-lg-6 col-md-12">
									<label>Inscrição Estadual</label>
									<input type="text" name="inscricao" attrname="est" placeholder="Inscrição Estadual">
								</div>
								<div class="col-lg-6 col-md-12">
									<label>Responsável</label>
									<input type="text" name="nome" placeholder="Nome do Responsável">
								</div>
								<div class="col-lg-6 col-md-12">
									<label>Telefone</label>
									<input type="text" name="telefone" attrname ="telephone1" placeholder="Telefone">
								</div>
								<div class="col-lg-6 col-md-12">
									<label>E-mail</label>
									<input type="text" name="email" placeholder="E-mail">
								</div>
								<div class="col-lg-6 col-md-12">
									<label>Endereço</label>
									<input type="text" name="endereco" placeholder="Endereço">
								</div>
								<div class="col-lg-6 col-md-12">
									<label>Estado</label>
									<select name="estado" class="form-control estado_uf" id="estado" data-cityField="#cidade_estado">
	                        			<option value="">Selecione um estado</option>
		                    		</select>
								</div>
								<div class="col-lg-6 col-md-12">
									<label>Cidade</label>
									<select name="cidade" class="form-control" id="cidade_estado" disabled>
                                		<option value="">Selecione uma cidade</option>
                            		</select>
								</div>
								<div class="col-lg-12 col-md-12">
								<label>Mensagem</label>
									<textarea name="mensagem" placeholder="Sua mensagem"></textarea>
								</div>
                                <div class="check"><input type="checkbox" name="check" id="check" value="concordo" checked>Li e concordo com a <a href="/site/termo/POLITICADEPRIVACIDADE.pdf">Política de privacidade</a></div>
								<input type="hidden" name="lang" value="pt">
								<input type="hidden" name="action_by_user" value="revendedor">
								<div class="col-lg-12 col-md-12">
									<button type="button" id="enviar_mensagem" class="site-btn">Enviar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	<!--INGLÊS-->	
	<?php }elseif ($idioma == 'ing'){ ?>
		<section class="page-section pt100">
			<div class="container pb100">
				<div class="section-title pt-5">
					<h1>Send a message, become a Caemmun reseller!</h1>
				</div>
				<div class="row">
					<div class="col-lg-3 contact-info mb-5 mb-lg-0">
						<h2>Industrial Unity in Arapongas, Paraná </h2>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i> Juriti Vermelha street, 279, Industrial Park V, <br>ZIP CODE 86702-280, Arapongas - PR, Brazil. </p>
						<p><i class="fa fa-phone"></i>+55 (43) 3172-6300</p>
						<p><i class="fa fa-at"></i> caemmun@caemmun.com.br</p>
					</div>
					<div class="col-lg-9">
						<form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <label>Company name</label>
                                    <input type="text" name="razao_social" placeholder="Company name">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Fantasy name</label>
                                    <input type="text" name="nome_fantasia" placeholder="Fantasy name">
                                </div>  
                                <div class="col-lg-6 col-md-12">
                                    <label>EIN</label>
                                    <input type="text" name="cnpj" placeholder="EIN">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>State registration</label>
                                    <input type="text" name="inscricao" attrname="est" placeholder="State registration">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Responsible</label>
                                    <input type="text" name="nome" placeholder="Responsible">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Phone number</label>
                                    <input type="text" name="telefone" attrname ="telephone1" placeholder="Phone number">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>E-mail</label>
                                    <input type="text" name="email" placeholder="E-mail">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Address</label>
                                    <input type="text" name="endereco" placeholder="Address">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>State/Region </label>
                                    <input type="text" name="estado" placeholder="State/Region">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>City</label>
                                    <input type="text" name="cidade" placeholder="City">
                                </div>
                                <div class="col-lg-12 col-md-12">
                                <label>Message</label>
                                    <textarea name="mensagem" placeholder="Message"></textarea>
                                </div>
                                <input type="hidden" name="lang" value="ing">
                                <input type="hidden" name="action_by_user" value="revendedor">
                                <div class="col-lg-12 col-md-12">
                                    <button type="button" id="enviar_mensagem" class="site-btn">Send</button>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</section>				
	<!--ESPANHOL-->	
	<?php }elseif ($idioma == 'esp'){ ?>
		<section class="page-section pt100">
			<div class="container pb100">
				<div class="section-title pt-5">
					<h1>Envíe un mensaje, sea un distribuidor de Caemmun.</h1>
				</div>
				<div class="row">
					<div class="col-lg-3 contact-info mb-5 mb-lg-0">
						<h2>UNIDAD INDUSTRIAL DE ARAPONGAS, PR</h2>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i> Calle Juriti Vermelha, 279, Parque Industrial V, <br>Código Postal 86702-280, Arapongas - PR, Brasil.</p>
						<p><i class="fa fa-phone"></i>+55 (43) 3172-6300</p>
						<p><i class="fa fa-at"></i> caemmun@caemmun.com.br</p>
					</div>
					<div class="col-lg-9">
						<form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <label>Razón social</label>
                                    <input type="text" name="razao_social" placeholder="Razón social">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Nombre de fantasía</label>
                                    <input type="text" name="nome_fantasia" placeholder="Nombre de fantasía">
                                </div>  
                                <div class="col-lg-6 col-md-12">
                                    <label>NIF</label>
                                    <input type="text" name="cnpj"  placeholder="NIF">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Registro estatal</label>
                                    <input type="text" name="inscricao" attrname="est" placeholder="Registro estatal">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Responsable</label>
                                    <input type="text" name="nome" placeholder="Responsable">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Teléfono</label>
                                    <input type="text" name="telefone" attrname ="telephone1" placeholder="Teléfono">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Correo electrónico</label>
                                    <input type="text" name="email" placeholder="Correo electrónico">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Dirección</label>
                                    <input type="text" name="endereco" placeholder="Dirección">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Estado  </label>
                                    <input type="text" name="estado" placeholder="Estado ">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Ciudad</label>
                                    <input type="text" name="cidade" placeholder="Ciudad">
                                </div>
                                <div class="col-lg-12 col-md-12">
                                <label>Mensaje</label>
                                    <textarea name="mensagem" placeholder="Mensaje"></textarea>
                                </div>
                                <input type="hidden" name="lang" value="esp">
                                <input type="hidden" name="action_by_user" value="revendedor">
                                <div class="col-lg-12 col-md-12">
                                    <button type="button" id="enviar_mensagem" class="site-btn">Enviar</button>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</section>						
	<?php }elseif ($idioma == 'fra'){ ?>
        <section class="page-section pt100">
            <div class="container pb100">
                <div class="section-title pt-5">
                    <h1>Envoyez–nous um message. Devenez un revendeur Caemmun!</h1>
                </div>
                <div class="row">
                    <div class="col-lg-3 contact-info mb-5 mb-lg-0">
                        <h2>UNITÉ INDUSTRIELLE ARAPONGAS, PR</h2>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> Rue Juriti Vermelha, 279, Parc industriel V, <br>Code postal 86702-280, Arapongas - PR, Brésil.</p>
                        <p><i class="fa fa-phone"></i>+55 (43) 3172-6300</p>
                        <p><i class="fa fa-at"></i> caemmun@caemmun.com.br</p>
                    </div>
                    <div class="col-lg-9">
                        <form class="contact-form" action="?" onsubmit="return false" id="contact-form" method="post" novalidate="novalidate">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <label>Raison social</label>
                                    <input type="text" name="razao_social" placeholder="Raison social">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Nom commercial</label>
                                    <input type="text" name="nome_fantasia" placeholder="Nom commercial">
                                </div>  
                                <div class="col-lg-6 col-md-12">
                                    <label>CNPJ</label>
                                    <input type="text" name="cnpj"  placeholder="CNPJ">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Enregistrement national</label>
                                    <input type="text" name="inscricao" attrname="est" placeholder="Enregistrement national">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Responsable</label>
                                    <input type="text" name="nome" placeholder="Responsable">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Téléphone</label>
                                    <input type="text" name="telefone" attrname ="telephone1" placeholder="Téléphone">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>E-mail</label>
                                    <input type="text" name="email" placeholder="E-mail">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Adresse</label>
                                    <input type="text" name="endereco" placeholder="Adresse">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Ètat  </label>
                                    <input type="text" name="estado" placeholder="Ètat ">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label>Ville </label>
                                    <input type="text" name="cidade" placeholder="Ville ">
                                </div>
                                <div class="col-lg-12 col-md-12">
                                <label>Message</label>
                                    <textarea name="mensagem" placeholder="Message"></textarea>
                                </div>
                                <input type="hidden" name="lang" value="fra">
                                <input type="hidden" name="action_by_user" value="revendedor">
                                <div class="col-lg-12 col-md-12">
                                    <button type="button" id="enviar_mensagem" class="site-btn">Envoyer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>                      
    <?php } ?>



	<?php include 'includes/footer_vazio.php'; ?>
	<?php include 'includes/scripts.php'; ?>

	<?php if ($idioma == 'pt'){ ?>

		<script type="text/javascript">
	        function inputHandler(masks, max, event) {
            var c = event.target;
            var v = c.value.replace(/\D/g, '');
            var m = c.value.length > max ? 1 : 0;
            VMasker(c).unMask();
            VMasker(c).maskPattern(masks[m]);
            c.value = VMasker.toPattern(v, masks[m]);
            }

            var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
            var tel = document.querySelector('input[attrname=telephone1]');
            VMasker(tel).maskPattern(telMask[0]);
            tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);

            var cnpjMask = ['99.999.999/9999-99'];
            var cnpj = document.querySelector('input[attrname=cnpj]');
            VMasker(cnpj).maskPattern(cnpjMask[0]);
            cnpj.addEventListener('input', inputHandler.bind(undefined, cnpjMask, 16), false);

            var estMask = ['999.999.999.999'];
            var est = document.querySelector('input[attrname=est]');
            VMasker(est).maskPattern(estMask[0]);
            est.addEventListener('input', inputHandler.bind(undefined, estMask, 15), false);

		</script>

	<?php }elseif ($idioma == 'ing'){ ?>

		<script type="text/javascript">
	        function inputHandler(masks, max, event) {
            var c = event.target;
            var v = c.value.replace(/\D/g, '');
            var m = c.value.length > max ? 1 : 0;
            VMasker(c).unMask();
            VMasker(c).maskPattern(masks[m]);
            c.value = VMasker.toPattern(v, masks[m]);
            }

            var telMask = ['+9 (999) 999-9999'];
            var tel = document.querySelector('input[attrname=telephone1]');
            VMasker(tel).maskPattern(telMask[0]);
            tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
		</script>

	<?php }elseif ($idioma == 'esp'){ ?>

	<?php }elseif ($idioma == 'fra'){ ?>

        <script type="text/javascript">
            function inputHandler(masks, max, event) {
            var c = event.target;
            var v = c.value.replace(/\D/g, '');
            var m = c.value.length > max ? 1 : 0;
            VMasker(c).unMask();
            VMasker(c).maskPattern(masks[m]);
            c.value = VMasker.toPattern(v, masks[m]);
            }

            var telMask = ['+99 9 99 99 99 99'];
            var tel = document.querySelector('input[attrname=telephone1]');
            VMasker(tel).maskPattern(telMask[0]);
            tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
        </script>

    <?php } ?>

	<script>
        var $form_contato = $('#contact-form');

        $(document).ready(function(){
            $form_contato.find('input, select, textarea').not('input[name=action_by_user], input[name=lang], input[name=check]').val('');                
        })                                                        
        
        function validateForm($form)
        {
            var all_inputs = $form_contato.find('input, select, textarea, input[name=check]').not('input[name=action_by_user], input[name=lang]');                
            var $input;
            var valid = {valid: true, error: ''};

            all_inputs.each(function(index){
                $input = $(all_inputs[index]);

                if($input.val() == '')
                {   
                    console.log($input);
                    valid.valid = false;
                    valid.error = 'form';

                    $input.addClass('has-error');                        
                }
                else
                {
                    $input.removeClass('has-error');
                }
            })    
            
            return valid;
        }            
        
        $(document).on('click', '#enviar_mensagem', function(){                
            var form_valid = validateForm($form_contato);

            if(form_valid.valid)    
            {
                var form_data = new FormData($form_contato[0]);

                var $button = $('#enviar_mensagem');
                
                $.ajax({
                    url: '<?= URL_BASE_ENVIO_AJAX ?>',
                    data: form_data,
                    type: 'POST',
                    processData: false,
                    contentType: false,

                    success: function(response){

                        response = JSON.parse(response);

                        $button.prop('disabled', true);

                        if(response.status == 'success')
                        {
                            swal({
                                title: 'Mensagem enviada com sucesso!',
                                text: 'Entraremos em contato em breve.',
                                type: 'success',
                                allowOutsideClick: false,
                            },
                            function (isConfirm) 
                            {
                                if(isConfirm)
                                {
                                    window.location.href = "home";
                                }
                            });                      
                        }
                        else
                        {
                            swal({
                                title: 'A mensagem não foi enviada!',
                                text: 'Tente novamente, se o problema persistir entre em contato através do nosso número: +55 (43) 3172-6300',
                                type: 'error'
                            });                      
                        }
                    },
                    error: function()
                    {
                        swal({
                            title: 'A mensagem não foi enviada!',
                            text: 'Tente novamente, se o problema persistir entre em contato através do nosso número: +55 (43) 3172-6300',
                            type: 'error'
                        }); 
                    }        
                })
            }
            else
            {
                if(form_valid.error == 'form')
                {
                    swal({
                        title: 'Ops!',
                        text: 'Certifique-se de completar todos os campos requeridos.',
                        type: 'error'
                    });    
                    
                }
            }
            
        })
        
    </script>
    <script>
		$(document).ready(function(){    
    $.ajax({
        type: 'POST',
        url:  'json_cidades/cidades_estados.json',
        success : function(data){

            if(!localStorage.getItem('estado_cidade')){
                var my_obj_serialized = JSON.stringify(data);
                localStorage.setItem("estado_cidade", my_obj_serialized);                   
            }

            var $selects = $('.estado_uf');               
            top.estados_cidades = JSON.parse(localStorage.getItem('estado_cidade'));

            for(key in top.estados_cidades){                 
                $selects.append('<option value="' + top.estados_cidades[key][1] + '" attr-id="'+key.replace('e', '')+'">' + top.estados_cidades[key][0] + '</option>'); 
            }

        },
        error: function(){ 
            console.log("Erro ao carregar estados");
        }
    })        
})

//CIDADES E ESTADOS 
$(document).on('change', '.estado_uf', function (e){    
    var $select_estado = $(this);
    var id_estado = $('option:selected', this).attr('attr-id');

    var $select_cidades = $($select_estado.attr('data-cityField'));
    var cid_element = $select_cidades[0];

    $select_cidades.empty();

    if(id_estado != ""){
        $select_cidades.prop('disabled', false);
        $select_cidades.append(new Option('Selecione uma cidade', ''));         

        for(var key in top.estados_cidades["e" + id_estado][2]){
            var cidade = top.estados_cidades["e" + id_estado][2][key];
            var option = document.createElement("option");
            console.log(cidade);
            option.value = cidade[1];
            option.text = cidade[1];
            cid_element.add(option);
        }

    }else{        
        $select_cidades.prop('disabled', true);
    }
})
</script>
<script type="text/javascript">
    $(document).on('change', '#check', function() {
        if(this.checked) {
            $('#enviar_mensagem').prop('disabled', false);
        }else{
            $('#enviar_mensagem').prop('disabled', true);
        }
    });
</script>
</body>
</html>