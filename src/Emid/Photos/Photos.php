<?php

namespace Emid\Photos;

class Photos
{

	/**
	 * ID vinculado ao módulo
	 * @var integer
	 */
	private $vinculo_id;

	/**
	 * Slug do módulo
	 * @var string
	 */
	private $modulo;

	/**
	 * SQL para ser usada com sprintf
	 * @var string
	 */
	private $sql;

	/**
	 * Constantes com nome dos paths. Modo de utilização: Fotos::THUMB
	 */
	const THUMB 	= 'thumbnails';
	const MEDIA 	= 'media';
	const LARGE 	= 'large';
	const ORIGINAL  = 'original';

	/**
	 * Método invocado ao ser instanciado
	 * @param int 	  $vinculo_id   id do registro
	 * @param string  $modulo       slug do módulo
	 * @return object $this 	    retorna a própria instância
	 */
	public function __construct($vinculo_id, $modulo)
	{
		$this->vinculo_id = $vinculo_id;
		$this->modulo     = $modulo;

		$this->sql = "SELECT *
				  	  FROM tb_fotos
				  	  WHERE fot_modulo = '%s'
				  	  AND fot_vinculo = %d
				  	  %s
				  	  ORDER BY fot_capa DESC, fot_ordem ASC";

		return $this;
	}

	/**
	 * Capa do registro
	 * @return string/null Nome da foto ou null quando não for encontrado
	 */
	public function getCapa($mysqli)
	{
		$query = mysqli_query($mysqli,  sprintf($this->sql, $this->modulo, $this->vinculo_id, "AND fot_capa = 1") );

		if( mysqli_num_rows($query) ){
			$row = mysqli_fetch_assoc( $query );
			return $row['fot_titulo'];
		}

		return null;
	}

	/**
	 * Todas as fotos do registro
	 * @return array    Nome das fotos
	 */
	public function getAll()
	{
		$result = array();

		$query = mysql_query( sprintf($this->sql, $this->modulo, $this->vinculo_id, "") );

		if( mysql_num_rows($query) ){
			while( $row = mysql_fetch_assoc($query) ){
				$result[] = $row['fot_titulo'];
			}
		}

		return array_filter( $result );
	}

	/**
	 * Todas as fotos do registro com legenda
	 * @return array    Nome das fotos
	 */
	public function getAllWithLegend()
	{
		$result = array();

		$query = mysql_query( sprintf($this->sql, $this->modulo, $this->vinculo_id, "") );

		if( mysql_num_rows($query) ){
			while( $row = mysql_fetch_assoc($query) ){
				$result[] = array('src' => $row['fot_titulo'], 'legend' => $row['fot_legenda']);
			}
		}

		return array_filter( $result );
	}

}